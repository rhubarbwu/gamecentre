package fall2018.csc2017.slidingtiles;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class BoardManagerTest {

    BoardManager solved4Man;
    BoardManager solved5Man;
    BoardManager unsolved5Man;

    SlidingTilesBoard solved4Board;
    SlidingTilesBoard solved5Board;
    SlidingTilesBoard unsolved5Board;

    @Before
    public void setUp() throws Exception {
        List<SlidingTile> tiles4 = new ArrayList();
        for (int tileNum = 0; tileNum != 16; tileNum++) {
            tiles4.add(new SlidingTile(tileNum, 4, 4));
        }
        solved4Board = new SlidingTilesBoard(tiles4);

        List<SlidingTile> tiles5 = new ArrayList();
        for (int tileNum = 0; tileNum != 25; tileNum++) {
            tiles5.add(new SlidingTile(tileNum, 5, 5));
        }
        solved5Board = new SlidingTilesBoard(tiles5, 5, 5);

        List<SlidingTile> tiles5uns = (List<SlidingTile>) ((ArrayList<SlidingTile>) tiles5).clone();
        Collections.reverse(tiles5uns);

        unsolved5Board = new SlidingTilesBoard(tiles5uns, 5, 5);

        solved4Man = new BoardManager(solved4Board);
        solved5Man = new BoardManager(solved5Board);
        unsolved5Man = new BoardManager(unsolved5Board);
    }

    @Test
    public void getSlidingTilesBoard() {
        assertEquals(solved4Man.getSlidingTilesBoard(), solved4Board);
        assertNotEquals(solved4Man.getSlidingTilesBoard(), unsolved5Board);
    }

    @Test
    public void puzzleSolved() {
        assertTrue(solved4Man.puzzleSolved());
        assertTrue(solved5Man.puzzleSolved());
        assertFalse(unsolved5Man.puzzleSolved());
    }

    @Test
    public void isValidTap() {
        assertTrue(solved4Man.isValidTap(14));
        assertFalse(solved4Man.isValidTap(15));
        assertFalse(solved4Man.isValidTap(1));
    }

    @Test
    public void touchMove() {
        solved4Man.touchMove(0);
        assertEquals(solved4Board.getTile(0, 0).getId(), 1);
        solved4Man.touchMove(14);
        assertEquals(solved4Board.getTile(3, 3).getId(), 15);
        assertEquals(solved4Board.getTile(3, 2).getId(), 16);
        solved5Man.touchMove(23);
        assertEquals(solved5Board.getTile(4, 4).getId(), 24);
        assertEquals(solved5Board.getTile(4, 3).getId(), 25);
    }

    @Test
    public void undo() {
        solved4Man.touchMove(14);
        solved4Man.touchMove(13);
        solved4Man.undo();
        solved4Man.undo();
        for (int i = 0; i < 16; i++) {
            assertEquals(solved4Board.getTile(i / 4, i % 4).getId(), i + 1);
        }

    }

    @Test
    public void getScore() {
        assertEquals(solved4Man.getScore(), 1280);
        solved4Man.touchMove(0);
        assertEquals(solved4Man.getScore(), 1280);
        solved4Man.touchMove(14);
        assertEquals(solved4Man.getScore(), 1279);
        solved4Man.undo();
        assertEquals(solved4Man.getScore(), 1278);
        solved4Man.undo();
        assertEquals(solved4Man.getScore(), 1278);
    }

    @Test
    public void editLifetime() {
        BoardManager test = new BoardManager();
        assertEquals(test.getLifetime(), 0);
        test.setLifetime(100L);
        assertEquals(test.getLifetime(), 100L);
    }
}
