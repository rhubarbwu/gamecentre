package fall2018.csc2017.slidingtiles;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class SlidingTilesBoardAndTileTest {

    /**
     * The board manager for testing.
     */
    BoardManager boardManager;

    /**
     * Make a set of tiles that are in order.
     *
     * @return a set of tiles that are in order
     */
    private List<SlidingTile> makeTiles() {
        List<SlidingTile> tiles = new ArrayList<>();
        final int numTiles = SlidingTilesBoard.DEFAULT_NUM_ROWS * SlidingTilesBoard.DEFAULT_NUM_COLS;
        for (int tileNum = 0; tileNum != numTiles; tileNum++) {
            tiles.add(new SlidingTile(tileNum + 1, tileNum));
        }

        return tiles;
    }

    /**
     * Make a solved SlidingTilesBoard.
     */
    private void setUpCorrect() {
        List<SlidingTile> tiles = makeTiles();
        SlidingTilesBoard slidingTilesBoard = new SlidingTilesBoard(tiles);
        boardManager = new BoardManager(slidingTilesBoard);
    }

    /**
     * Shuffle a few tiles.
     */
    private void swapFirstTwoTiles() {
        boardManager.getSlidingTilesBoard().swapTiles(0, 0, 0, 1);
    }

    /**
     * Test whether swapping two tiles makes a solved board unsolved.
     */
    @Test
    public void testIsSolved() {
        setUpCorrect();
        swapFirstTwoTiles();
        assertEquals(false, boardManager.puzzleSolved());
        swapFirstTwoTiles();
        assertEquals(true, boardManager.puzzleSolved());
        // program should no longer care what moves you do
        swapFirstTwoTiles();
        assertEquals(true, boardManager.puzzleSolved());
    }

    /**
     * Test whether swapping the first two tiles works.
     */
    @Test
    public void testSwapFirstTwo() {
        setUpCorrect();
        assertEquals(1, boardManager.getSlidingTilesBoard().getTile(0, 0).getId());
        assertEquals(2, boardManager.getSlidingTilesBoard().getTile(0, 1).getId());
        boardManager.getSlidingTilesBoard().swapTiles(0, 0, 0, 1);
        assertEquals(2, boardManager.getSlidingTilesBoard().getTile(0, 0).getId());
        assertEquals(1, boardManager.getSlidingTilesBoard().getTile(0, 1).getId());
    }

    /**
     * Test whether swapping the last two tiles works.
     */
    @Test
    public void testSwapLastTwo() {
        setUpCorrect();
        assertEquals(15, boardManager.getSlidingTilesBoard().getTile(3, 2).getId());
        assertEquals(16, boardManager.getSlidingTilesBoard().getTile(3, 3).getId());
        boardManager.getSlidingTilesBoard().swapTiles(3, 3, 3, 2);
        assertEquals(16, boardManager.getSlidingTilesBoard().getTile(3, 2).getId());
        assertEquals(15, boardManager.getSlidingTilesBoard().getTile(3, 3).getId());
    }

    /**
     * Test whether isValidHelp works.
     */
    @Test
    public void testIsValidTap() {
        setUpCorrect();
        assertEquals(true, boardManager.isValidTap(11));
        assertEquals(false, boardManager.isValidTap(10));
    }
}

