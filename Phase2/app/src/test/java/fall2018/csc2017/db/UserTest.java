package fall2018.csc2017.db;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;

/**
 * We mainly care about the user's static method here, but we'll test the others
 */
@RunWith(RobolectricTestRunner.class)
public class UserTest {

    /**
     * It's a bit difficult to test *EVERY* single sha256sum, but we just want to make sure it's
     * at least the right algorithm and that it's not crashing
     *
     * @throws NoSuchAlgorithmException if your system doesn't support SHA-256
     */
    @Test
    public void verifySha256() throws NoSuchAlgorithmException {
        assertEquals("0d68adccbe1d35db59a585f24f91b12e57bf41cbd017ee1aee66023f088356a6",
                User.hashPassword("for all epsilon, exists some delta greater than zero"));
    }

    @Test
    public void verifyStaticSetter() {
        User target = new User("epsilon", "neverdelta");
        User.setSignedInUser(target);
        assert (target.equals(User.getSignedInUser()));
    }

    @Test
    public void verifyEquals() {
        User u1 = new User("aries", "piazza");
        User u2 = new User("tony", "picasso");
        // the DB will prohibit two users from sharing the same username, but the java rep isn't
        // guaranteed to be unique
        User u1c = new User("aries", "epsilon");
        // will not work with default equals, only will work if the prog only checks username eq
        assertEquals(u1, u1c);
        assertNotEquals(u1, u2);
    }
}
