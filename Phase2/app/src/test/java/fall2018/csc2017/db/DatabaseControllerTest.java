package fall2018.csc2017.db;

import android.content.Context;

import java.io.IOException;

import android.app.Activity;

import fall2018.csc2017.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.After;

import static org.junit.Assert.*;

import org.junit.runner.RunWith;

import org.robolectric.*;

@RunWith(RobolectricTestRunner.class)
public class DatabaseControllerTest {

    final static Context context = RuntimeEnvironment.application;

    @Test
    public void testController() {
        DatabaseController.init(context.getApplicationContext());
        assertNotNull(DatabaseController.provideDatabase());
        DatabaseController.kill();
        assertNull(DatabaseController.provideDatabase());
    }

}