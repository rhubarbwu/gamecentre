package fall2018.csc2017.db;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class DatabaseDateConvTest {

    @Test
    public void ensureConstructorIsntRun() {
        try {
            DatabaseDateConv ddc = new DatabaseDateConv();
            // fail if no err
            assertFalse(true);
        } catch (Exception e) {
        }
    }

    @Test
    public void testFromTimestamp() {
        Date target = new Date(1000L);
        assertEquals(DatabaseDateConv.fromTimestamp(1000L), target);
    }

    @Test
    public void testDateToTimestamp() {
        Long target = 1000L;
        assertEquals(DatabaseDateConv.dateToTimestamp(new Date(target)), target);
    }
}