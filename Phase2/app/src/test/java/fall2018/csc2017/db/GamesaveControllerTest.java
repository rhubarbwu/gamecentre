package fall2018.csc2017.db;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import android.app.Application;
import android.content.Context;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import fall2018.csc2017.GameSelectActivity;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(application = Application.class)
public class GamesaveControllerTest {

    final static Context context = RuntimeEnvironment.application;

    @Before
    public void init() {
        // We need to have the database ready
        DatabaseController.init(context.getApplicationContext());
        UserController.createUser("epsilon", "mS");
    }

    @After
    public void rm() {
        DatabaseController.reset();
        DatabaseController.kill();
    }

    @Test
    public void directEntryTest() throws Exception {
        GamesaveController.createGamesave("epsilon", Games.SLIDING_TILES.getValue(), "oof");
        assertEquals((String)
                GamesaveController.getGamesaveData("epsilon", Games.SLIDING_TILES.getValue()), "oof");
    }

}
