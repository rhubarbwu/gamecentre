package fall2018.csc2017.pipes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PipeTileTest {
    private PipeTile emptyTile, borderTile, turnTile, nailTile;

    @Before
    public void setup() {
        emptyTile = new PipeTile(0, 0);
        borderTile = new PipeTile(1, 1);
        turnTile = new PipeTile(4, 0);
        nailTile = new PipeTile(5, 2);
    }

    @Test
    public void testEmptyTileConnections() {
        assertEquals("0000", emptyTile.getConnections());
    }

    @Test
    public void testBorderTileConnections() {
        assertEquals("0101", borderTile.getConnections());
    }

    @Test
    public void testBorderTileRotatable() {
        assertFalse(borderTile.isRotatable());
    }

    @Test
    public void testBorderTileType() {
        assertEquals(1, borderTile.getType());
    }

    @Test
    public void testTurnPipeRotatable() {
        assertTrue(turnTile.isRotatable());
    }

    @Test
    public void testTurnPipeConnections() {
        String beforeConnections = turnTile.getConnections();
        String expectedConnections = beforeConnections.substring(3, 4) + beforeConnections.substring(0, 3);
        turnTile.rotate(false);
        assertEquals(expectedConnections, turnTile.getConnections());
    }

    @Test
    public void testNailPipeConnections() {
        String beforeConnections = nailTile.getConnections();
        String expectedConnections = beforeConnections.substring(1) + beforeConnections.substring(0, 1);
        nailTile.rotate(true);
        assertEquals(expectedConnections, nailTile.getConnections());
    }
}