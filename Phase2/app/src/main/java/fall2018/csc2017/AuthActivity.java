package fall2018.csc2017;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;

import fall2018.csc2017.db.UserController;
import fall2018.csc2017.db.User;

public class AuthActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_auth);
        addLoginButtonListener();
        addRegisterButtonListener();
    }

    /**
     * Listener for the login button. Login a user if the specified username and password match in the database.
     */
    private void addLoginButtonListener() {
        Button loginButton = findViewById(R.id.LoginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = getInputUsername();
                String password = getInputPassword();
                String hashedPassword = null;

                try {
                    hashedPassword = User.hashPassword(password);
                } catch (NoSuchAlgorithmException nsae) {
                    Toast.makeText(v.getContext(), "Internal error, please try again", Toast.LENGTH_LONG).show();
                    return;
                }

                User user = UserController.authenticateUser(username, hashedPassword);
                if (user == null) {
                    Toast.makeText(v.getContext(), "Invalid username or password", Toast.LENGTH_LONG).show();
                } else {
                    User.setSignedInUser(user);

                    clearInputUsername();
                    clearInputPassword();

                    Intent nextActivityIntent = new Intent(AuthActivity.this, GameSelectActivity.class);
                    AuthActivity.this.startActivity(nextActivityIntent);
                }
            }
        });
    }

    /**
     * Listener for the register button. Registers a new user when clicked if the user does not already exist.
     */
    private void addRegisterButtonListener() {
        Button registerButton = findViewById(R.id.RegButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = getInputUsername();
                String password = getInputPassword();
                String hashedPassword = null;

                if (username == null || username.isEmpty()) {
                    Toast.makeText(v.getContext(), "Username can't be empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if (password == null || password.isEmpty()) {
                    Toast.makeText(v.getContext(), "Password can't be empty", Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    hashedPassword = User.hashPassword(password);
                } catch (NoSuchAlgorithmException nsae) {
                    Toast.makeText(v.getContext(), "Internal error, please try again", Toast.LENGTH_LONG).show();
                    return;
                }

                User newUser = new User(username, hashedPassword);

                try {
                    UserController.createUser(newUser);
                } catch (SQLiteConstraintException constraintException) {
                    Toast.makeText(v.getContext(), "User already exists, please login", Toast.LENGTH_LONG).show();
                    return;
                }

                User.setSignedInUser(newUser);

                clearInputUsername();
                clearInputPassword();

                Intent nextActivityIntent = new Intent(AuthActivity.this, GameSelectActivity.class);
                AuthActivity.this.startActivity(nextActivityIntent);
            }
        });
    }

    /**
     * Returns the username inputted by the user.
     *
     * @return the text in the username edit text
     */
    private String getInputUsername() {
        EditText usernameEditText = findViewById(R.id.UsernameEditText);
        return usernameEditText.getText().toString();
    }

    /**
     * Clears the username edit text.
     */
    private void clearInputUsername() {
        EditText usernameEditText = findViewById(R.id.UsernameEditText);
        usernameEditText.setText("");
    }

    /**
     * Returns the password inputted by the user.
     *
     * @return the text in the password edit text
     */
    private String getInputPassword() {
        EditText passwordEditText = findViewById(R.id.PasswordEditText);
        return passwordEditText.getText().toString();
    }

    /**
     * Clears the password edit text.
     */
    private void clearInputPassword() {
        EditText passwordEditText = findViewById(R.id.PasswordEditText);
        passwordEditText.setText("");
    }
}
