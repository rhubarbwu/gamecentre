package fall2018.csc2017;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;


import fall2018.csc2017.db.Games;
import fall2018.csc2017.db.Score;
import fall2018.csc2017.db.ScoreController;

public class ScoreboardActivity extends AppCompatActivity {
    private ListView listView;
    private CheckBox checkBox;
    private Score[] scores;
    private ArrayAdapter<Score> adaptToListView;
    private Games game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        game = Games.values()[(int) getIntent().getSerializableExtra("gameId")];

        setContentView(R.layout.activity_scoreboard);
        listView = findViewById(R.id.ScoreboardList);
        checkBox = findViewById(R.id.SortByUserCheckBox);
        scores = ScoreController.sortByTimeTaken(game.getValue());

        setScoreboardGameTitle();
        updateListView();
        addSortByMovesRadioButtonListener();
        addSortByTimeTakenRadioButtonListener();
    }

    /**
     * Sets the title of the scoreboard according to the game.
     */
    private void setScoreboardGameTitle() {
        TextView title = findViewById(R.id.ScoreboardHeader);
        title.setText(game + " Scoreboard");
    }

    /**
     * Add listener for SortByTimeTaken Radio Button
     */
    private void addSortByTimeTakenRadioButtonListener() {
        RadioButton timeTakenRadioButton = findViewById(R.id.SortByTimeTakenRadioButton);
        timeTakenRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkBox.isChecked()) {
                    String username = getInputUsername();
                    scores = ScoreController.sortByTimeTaken(game.getValue(), username);
                } else {
                    scores = ScoreController.sortByTimeTaken(game.getValue());
                }
                updateListView();
            }
        });
    }

    /**
     * Add listener for SortByMoves Radio Button
     */
    private void addSortByMovesRadioButtonListener() {
        RadioButton timeTakenRadioButton = findViewById(R.id.SortByMovesRadioButton);
        timeTakenRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    String username = getInputUsername();
                    scores = ScoreController.sortByScore(game.getValue(), username);
                } else {
                    scores = ScoreController.sortByScore(game.getValue());
                }
                updateListView();

            }
        });
    }

    /**
     * Get the username typed into the textbox
     */
    private String getInputUsername() {
        EditText usernameEditText = findViewById(R.id.UsernameEditText);
        return usernameEditText.getText().toString();
    }

    /**
     * Get the password typed into the textbox
     */
    private void updateListView() {
        adaptToListView = new ArrayAdapter<Score>(this, android.R.layout.simple_list_item_1, scores);
        listView.setAdapter(adaptToListView);
    }
}
