package fall2018.csc2017.db;

/**
 * Abstraction layer to avoid exposing the database to the rest of the app.
 */
public class UserController {
    /**
     * Creates a new user in the database. Throws an exception when the user already exists.
     *
     * @param user the user to be created
     * @throws Exception of whatever type the database gives, if a user already exists.
     */
    public static void createUser(User user) {
        UserDao uDao = DatabaseController.provideDatabase().userDao();
        uDao.createUser(user);
    }

    /**
     * Creates a new user in the database via the default User constructor. Also throws an exception if the user already exists.
     */
    public static void createUser(String username, String hashedPassword) {
        createUser(new User(username, hashedPassword));
    }

    /**
     * Authenticates a uses using the username and the hashed password passed in.
     *
     * @param username       the username of the user to be authenticated
     * @param hashedPassword the hashed password of the user to be authenticated
     * @return the user if the authentication is successful, null otherwise
     */
    public static User authenticateUser(String username, String hashedPassword) {
        UserDao userDao = DatabaseController.provideDatabase().userDao();
        return userDao.authenticateUser(username, hashedPassword);
    }
}
