package fall2018.csc2017.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.*;

/**
 * The main Room database for the app.
 */
@Database(entities = {User.class, Gamesave.class, Score.class}, version = 6)
@TypeConverters({DatabaseDateConv.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract ScoreDao scoreDao();

    public abstract GamesaveDao gamesaveDao();

    public abstract UserDao userDao();
}
