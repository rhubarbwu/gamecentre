package fall2018.csc2017.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

/**
 * The DAO interface for the User.
 */
@Dao
public interface UserDao {
    /**
     * Creates a new user in the database. Throws an exception when the user already exists.
     *
     * @param user the user to be created
     */
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void createUser(User user);

    /**
     * Authenticates a uses using the username and the hashed password passed in.
     *
     * @param username       the username of the user to be authenticated
     * @param hashedPassword the hashed password of the user to be authenticated
     * @return the user if the authentication is successful, null otherwise
     */
    @Query("SELECT * FROM user WHERE username = :username AND password = :hashedPassword LIMIT 1")
    User authenticateUser(String username, String hashedPassword);
}
