package fall2018.csc2017.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Model for a user.
 */
@Entity
public class User {
    /**
     * Username of the user.
     */
    @PrimaryKey
    @NonNull
    private String username;

    /**
     * Password of the user.
     */
    private String password;

    /**
     * Variable to hold the current signed in user.
     */
    private static User signedInUser;

    /**
     * The default constructor for this class.
     */
    public User() {
    }

    /**
     * Initializes the variables of the class.
     *
     * @param username       the username of the user
     * @param hashedPassword the hashed password of the user
     */
    public User(String username, String hashedPassword) {
        this.username = username;
        this.password = hashedPassword;
    }

    /**
     * Getter for the username.
     *
     * @return the username of the user
     */
    public String getUsername() {
        return username;
    }

    /**
     * Getter for the hashed password.
     *
     * @return the hashed password of the user
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for the username.
     *
     * @param username the username of the user
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Setter for the hashed password.
     *
     * @param password the hashed password of the user
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get the currently signed in user.
     *
     * @return the currently signed in user, or null if there is not one
     */
    public static User getSignedInUser() {
        return signedInUser;
    }

    /**
     * Sets the currently signed in user.
     *
     * @param signedInUser the currently signed in user
     */
    public static void setSignedInUser(User signedInUser) {
        User.signedInUser = signedInUser;
    }

    /**
     * Hashes the plain text password using the SHA-256 algorithm with a hexadecimal encoding.
     *
     * @param password the plain text password
     * @return the hexadecimal hashed password
     * @throws NoSuchAlgorithmException cryptographic algorithm is not available
     */
    public static String hashPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(password.getBytes());

        byte[] hashBytes = md.digest();

        StringBuffer hexString = new StringBuffer();
        for (Byte b : hashBytes) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

    /**
     * Returns true iff this User is the same as the other specified User.
     * @param obj the other user
     * @return if this user is the same as the other one.
     */
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof User)) {return false;}
        // We know usernames are the primary key, so they are unique.
        return username.equals(((User) obj).username);
    }
}
