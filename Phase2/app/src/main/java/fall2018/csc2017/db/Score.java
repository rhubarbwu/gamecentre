package fall2018.csc2017.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "username",
        childColumns = "username"),
        indices = {@Index(value = {"username", "gameId"})})

/**
 * Respresentation of a single Score for the database.
 */
public class Score {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int scoreid;
    @NonNull
    private String username;
    @NonNull
    private long timeTaken;
    @NonNull
    private Date time;
    @NonNull
    private int score;
    @NonNull
    private int gameId;

    /**
     * Creates a new score with date set to the current date.
     *
     * @param username  The username of the score's owner.
     * @param gameId    The ID of the game as specified in the Games class.
     * @param timeTaken The amount of time the user has taken to complete the game.
     * @param score     An integer representation of how "well" the user did. It is up to
     *                  the game to define what this actually means.
     */
    public Score(String username, int gameId, long timeTaken, int score) {
        this.username = username;
        this.gameId = gameId;
        this.timeTaken = timeTaken;
        this.time = new Date();
        this.score = score;
    }

    /**
     * Returns the game ID associated with this score.
     *
     * @return the game ID as specified in the Games class.
     */
    public int getGameId() {
        return gameId;
    }

    /**
     * Sets the new game ID for this score.
     *
     * @param gameId the new game ID
     */
    public void setGameId(@NonNull int gameId) {
        this.gameId = gameId;
    }

    /**
     * Returns the username of the score owner.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username for this score.
     *
     * @param username new username.
     */
    public void setUsername(@NonNull String username) {
        this.username = username;
    }

    /**
     * Returns the time taken for the user to complete the game
     *
     * @return the time taken in seconds
     */
    public long getTimeTaken() {
        return timeTaken;
    }

    /**
     * Sets the time taken for this score
     *
     * @param timeTaken new time taken.
     */
    public void setTimeTaken(@NonNull long timeTaken) {
        this.timeTaken = timeTaken;
    }

    /**
     * Returns the time of which the game was completed
     *
     * @return Date obj representing the time.
     */
    public Date getTime() {
        return time;
    }

    /**
     * Sets the new time for this Score.
     *
     * @param time New time.
     */
    public void setTime(@NonNull Date time) {
        this.time = time;
    }

    /**
     * Returns the numeric representation of the user's performance.
     *
     * @return an int, meaning dependant on game.
     */
    public int getScore() {
        return score;
    }

    /**
     * Returns the unique score ID. Never should be actually used.
     */
    public int getScoreid() {
        return scoreid;
    }

    /**
     * Sets a new score ID.
     *
     * @param newId the new ID.
     */
    public void setScoreid(int newId) {
        scoreid = newId;
    }

    /**
     * Changes the score of a given record.
     *
     * @param newScore the new integer score.
     */
    @NonNull
    public void setScore(int newScore) {
        this.score = newScore;
    }

    /**
     * Formats the score for the scoreboard.
     */
    @Override
    public String toString() {
        return username + " scored " + score + "p in " + (int) ((double) timeTaken / 1e9) + "s";
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Score) && (scoreid == ((Score) obj).scoreid);
    }
}
