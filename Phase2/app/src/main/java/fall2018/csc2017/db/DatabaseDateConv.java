package fall2018.csc2017.db;

import android.arch.persistence.room.*;

import java.util.Date;

/*
 * Code taken from Room documentation.
 * https://developer.android.com/training/data-storage/room/referencing-data
 */

/**
 * Converts the Date format into a Long value, which Room supports.
 */
public class DatabaseDateConv {

    public DatabaseDateConv() throws Exception {
        throw new Exception("Do not make any instances of this, these methods are static");
    }

    /**
     * Convert a Long value into a Date object.
     *
     * @param value the long value
     * @return the Date object representation
     */
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    /**
     * Convert a Date object to the DB's long format.
     *
     * @param date the date obj to return
     * @return the Long rep.
     */
    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
