package fall2018.csc2017.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;


/**
 * The Dao (Data Access Object) for the scores.
 */
@Dao
public interface ScoreDao {
    /**
     * Creates a new score entry in the activity_scoreboard.
     * Scores should be unique.
     *
     * @param score the new score entry.
     */
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void createScore(Score score);

    /**
     * Returns all the scores for a specified game, in descending order.
     * The entries are sorted by the score, which is defined differently for each game.
     *
     * @return the sorted array of scores
     */
    @Query("SELECT * FROM score WHERE gameId = :gameId ORDER BY score DESC")
    Score[] sortByScore(int gameId);

    /**
     * Returns all the scores for a specified game, in ascending order of time taken.
     *
     * @return the sorted array of scores
     */
    @Query("SELECT * FROM score WHERE gameId = :gameId ORDER BY timeTaken")
    Score[] sortByTimeTaken(int gameId);

    /**
     * Returns all the scores of a specified user for a specified game, in descending order.
     * Entries sorted by the actual score.
     *
     * @return the sorted array of scores from a given username.
     */
    @Query("SELECT * FROM score WHERE gameId = :gameId AND username = :username ORDER BY score DESC")
    Score[] sortByScore(int gameId, String username);

    /**
     * Returns all the scores of a specified user for a specified game, in ascending order.
     * Entries sorted by the time taken.
     *
     * @return the sorted array of scores from a given username.
     */
    @Query("SELECT * FROM score WHERE gameId = :gameId AND username = :username ORDER BY timeTaken")
    Score[] sortByTimeTaken(int gameId, String username);
}
