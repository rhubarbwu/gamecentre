package fall2018.csc2017.db;


/**
 * Enum stores all the games of this app, currently only has sliding tiles.
 */
public enum Games {
    SLIDING_TILES(0, "Sliding Tiles"),
    MINESWEEPER(1, "Minesweeper"),
    PIPES(2, "Pipes");

    /**
     * Score the int value of the game.
     */
    private final int value;

    /**
     * The name of this game.
     */
    private String gameName;

    /**
     * Constructor for the enum.
     *
     * @param value the value of the game
     */
    Games(int value, String gameName) {
        this.value = value;
        this.gameName = gameName;
    }

    /**
     * Get the value of the enum constant.
     *
     * @return the int value of the enum constant
     */
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return gameName;
    }
}
