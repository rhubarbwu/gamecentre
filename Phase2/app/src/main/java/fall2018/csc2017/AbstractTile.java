package fall2018.csc2017;

import android.support.annotation.NonNull;

import java.io.Serializable;

public abstract class AbstractTile implements Serializable {

    /**
     * The list of images available
     */
    public int[] TILES;

    /**
     * The background id to find the tile image.
     */
    public int background;

    /**
     * The unique id.
     */
    public int id;

    /**
     * Return the background id.
     *
     * @return the background id
     */
    public int getBackground() {
        return background;
    }

    /**
     * Return the tile id.
     *
     * @return the tile id
     */
    public int getId() {
        return id;
    }

    /**
     * A MinesweeperTile with id and background. The background may not have a corresponding image.
     *
     * @param id         the id
     * @param background the background
     */
    public AbstractTile(int id, int background) {
        this.id = id;
        this.background = background;
    }

    public AbstractTile() {
    }

    public int compareTo(@NonNull AbstractTile o) {
        return o.id - this.id;
    }
}
