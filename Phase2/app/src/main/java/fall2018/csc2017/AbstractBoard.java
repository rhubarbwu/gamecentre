package fall2018.csc2017;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.Observable;

/**
 * The abstract superclass for each game board.
 */
public abstract class AbstractBoard<T extends AbstractTile> extends Observable implements Serializable {

    /**
     * The number of rows and columns of this board.
     */
    public int numRows, numColumns;

    /**
     * The tiles on the slidingTilesBoard in row-major order.
     */
    public T[][] tiles;

    /**
     * Initializes the board with empty tiles.
     *
     * @param numRows    the number of rows this board has
     * @param numColumns the number of columns this board has
     */
    public AbstractBoard(Class<T> tileClass, int numRows, int numColumns) {
        this.numRows = numRows;
        this.numColumns = numColumns;
        this.tiles = (T[][]) Array.newInstance(tileClass, numRows, numColumns);
    }

    /**
     * Getter for the number of tiles.
     *
     * @return the number of tiles this board has
     */
    public int getNumTiles() {
        return numRows * numColumns;
    }

    /**
     * Returns a specific tile.
     *
     * @param row    the row of the desired tile
     * @param column the column of the desired tile
     * @return the tile at that row and column
     */
    public T getTile(int row, int column) {
        return tiles[row][column];
    }

    /**
     * Getter for the number of rows in this board.
     *
     * @return the number of rows in this board
     */
    public int getNumRows() {
        return numRows;
    }

    /**
     * Getter for the number of columns in this board.
     *
     * @return the number of columns in this board
     */
    public int getNumColumns() {
        return numColumns;
    }

    /**
     * Overrides iterator in Iterable interface. Iterates in row-major order.
     *
     * @return the iterator
     */
    @NonNull
    public Iterator<T> iterator() {
        return new BoardIterator(this);
    }

    /**
     * The iterator that the tiles on the board in row-major order.
     */
    public class BoardIterator implements Iterator<T> {
        private AbstractBoard<T> board;
        private int currPosition;

        /**
         * New iterator on the tiles of the board in row-major order.
         *
         * @param board the board to iterate on
         */
        public BoardIterator(AbstractBoard<T> board) {
            this.board = board;
            this.currPosition = 0;
        }

        /**
         * Returns whether there is a next tile on the board.
         *
         * @return true if there is a next tile, false otherwise
         */
        @Override
        public boolean hasNext() {
            return currPosition < board.getNumTiles();
        }

        /**
         * Returns the next tile on the board.
         *
         * @return the next tile on the board
         */
        @Override
        public T next() {
            int row = currPosition / board.getNumRows();
            int col = currPosition % board.getNumColumns();
            T tile = board.getTile(row, col);

            currPosition++;

            return tile;
        }

    }

}
