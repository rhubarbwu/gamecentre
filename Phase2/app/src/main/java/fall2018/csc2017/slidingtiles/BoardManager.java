package fall2018.csc2017.slidingtiles;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.List;


/**
 * Manage a slidingTilesBoard, including swapping tiles, checking for a win, and managing taps.
 */
public class BoardManager implements Serializable {

    /**
     * Boolean for whether or not this board is solved
     */
    private boolean puzzleSolved;

    /**
     * The slidingTilesBoard being managed.
     */
    private SlidingTilesBoard slidingTilesBoard;

    /**
     * Represents the opposite movements of whatever the user has done.
     * By doing so, we only need to store a single number for every undo we make.
     */
    private ArrayDeque<Integer> history;

    /**
     * Represents the number of moves the user has played.
     */
    private int moves;

    /**
     * Represents the lifetime of the slidingTilesBoard at it's creation.
     * May be overwritten by other classes, but stored in BoardManager to enable having it saved
     * internally.
     */
    private long lifetime;

    /**
     * Manage a slidingTilesBoard that has been pre-populated.
     *
     * @param slidingTilesBoard the slidingTilesBoard
     */
    BoardManager(SlidingTilesBoard slidingTilesBoard) {
        this.slidingTilesBoard = slidingTilesBoard;
        this.history = new ArrayDeque<Integer>();
        this.moves = 0;
        this.lifetime = 0;
    }

    /**
     * Return the current slidingTilesBoard.
     */
    SlidingTilesBoard getSlidingTilesBoard() {
        return slidingTilesBoard;
    }

    /**
     * Manage a new shuffled slidingTilesBoard of default dimensions.
     */
    public BoardManager() {
        this(SlidingTilesBoard.DEFAULT_NUM_ROWS, SlidingTilesBoard.DEFAULT_NUM_COLS);
    }

    /**
     * Manage a new shuffled slidingTilesBoard of specified dimensions.
     * <p>
     * Precondition: num_rows, num_cols > 0
     *
     * @param num_rows The number of rows for this slidingTilesBoard.
     * @param num_cols The number of columns for this slidingTilesBoard.
     */
    BoardManager(int num_rows, int num_cols) {
        this.slidingTilesBoard = new SlidingTilesBoard(generateSolvablePuzzle(num_rows, num_cols), num_rows, num_cols);
        this.history = new ArrayDeque();
        this.moves = 0;
        this.lifetime = 0;
    }

    /**
     * Return whether the tiles are in row-major order.
     *
     * @return whether the tiles are in row-major order
     */
    boolean puzzleSolved() {
        if (puzzleSolved) return true; // if this board is already solved, don't compute
        int currTileVal = 1;
        for (SlidingTile tile : slidingTilesBoard) {
            if (tile.getId() != currTileVal) {
                puzzleSolved = false;
                return false;
            }
            currTileVal++;
        }
        puzzleSolved = true;
        return true;
    }

    /**
     * Return whether any of the four surrounding tiles is the blank tile.
     *
     * @param position the tile to check
     * @return whether the tile at position is surrounded by a blank tile
     */
    boolean isValidTap(int position) {
        int row = position / slidingTilesBoard.getNumColumns();
        int col = position % slidingTilesBoard.getNumColumns();
        int blankId = slidingTilesBoard.getNumTiles();
        // Are any of the 4 the blank tile?
        SlidingTile above = row == 0 ? null : slidingTilesBoard.getTile(row - 1, col);
        SlidingTile below = row == slidingTilesBoard.getNumRows() - 1 ? null : slidingTilesBoard.getTile(row + 1, col);
        SlidingTile left = col == 0 ? null : slidingTilesBoard.getTile(row, col - 1);
        SlidingTile right = col == slidingTilesBoard.getNumColumns() - 1 ? null : slidingTilesBoard.getTile(row, col + 1);
        return (below != null && below.getId() == blankId)
                || (above != null && above.getId() == blankId)
                || (left != null && left.getId() == blankId)
                || (right != null && right.getId() == blankId);
    }

    /**
     * Process a touch at position in the slidingTilesBoard, swapping tiles as appropriate.
     *
     * @param position the position
     */
    void touchMove(int position) {

        int row = position / slidingTilesBoard.getNumRows();
        int col = position % slidingTilesBoard.getNumColumns();

        // Get the location of the blank tile
        int blankTilePosition = slidingTilesBoard.getBlankTileLocation(position);
        if (blankTilePosition >= 0) {
            int blankTileRow = blankTilePosition / slidingTilesBoard.getNumRows();
            int blankTileCol = blankTilePosition % slidingTilesBoard.getNumColumns();
            // Swap the tiles
            slidingTilesBoard.swapTiles(row, col, blankTileRow, blankTileCol);
            // Store the reverse of the swap.
            history.push(blankTilePosition);
            moves += 1;
        }
    }

    /**
     * Attempts to undo the last move. If there are no last moves remaining, does nothing.
     *
     * @return true iff the undo was done; false iff undo can't be done.
     */
    public boolean undo() {
        if (history.isEmpty()) {
            return false;
        } else {
            touchMove(history.pop());
            history.pop(); // pop the undo move itself
            // undos are a move, which comes with their appropiate penalty on score.
            return true;
        }
    }

    /**
     * Returns the current score of the slidingTilesBoard. The formula is somewhat arbitrary.
     */
    public int getScore() {
        return ((int) (Math.pow(slidingTilesBoard.getNumColumns(), 2) * Math.pow(slidingTilesBoard.getNumRows(), 2)) * 5) - moves;
    }

    /**
     * Returns the lifetime of this slidingTilesBoard at creation.
     * Unfortunately, this feature relies on GameActivity as BoardManagers should not be the
     * class that manages a timer
     * @return the lifetime
     */
    public long getLifetime() {
        return lifetime;
    }

    /**
     * Sets the new lifetime of this slidingTilesBoard for when it is to be saved.
     */
    public void setLifetime(long lifetime) {
        this.lifetime = lifetime;
    }

    /*
     * Generates a random List that represents a sliding puzzle.
     * This list is guaranteed to work.
     *
     * @param num_rows, num_cols - dimensions of the puzzle
     * @return a solvable SlidingTiles list.
     */
    private static List<SlidingTile> generateSolvablePuzzle(int num_rows, int num_cols) {
        // Implementation based on https://www.cs.bham.ac.uk/~mdr/teaching/modules04/java2/TilesSolvability.html
        List<SlidingTile> tiles = new ArrayList();
        final int numTiles = num_rows * num_cols;
        for (int tileNum = 0; tileNum < numTiles - 1; tileNum++) {
            tiles.add(new SlidingTile(tileNum, num_rows, num_cols));
        }
        Collections.shuffle(tiles);
        tiles.add(new SlidingTile(numTiles - 1, num_rows, num_cols));
        if (inversions(tiles) % 2 != 0) {
            makeSolvable(tiles);
        }

        return tiles;
    }

    /*
     * Calculate the number of inversions in a sliding tiles list.
     */
    private static int inversions(List<SlidingTile> llist) {
        // from: https://www.cs.bham.ac.uk/~mdr/teaching/modules04/java2/TilesSolvability.html
        int counter = 0;
        for (int i = 0; i < llist.size(); i++) {
            for (int j = i; j < llist.size(); j++) {
                if ((llist.get(j).getId() != llist.size()) && (llist.get(j).getId() > llist.get(i).getId())) {
                    counter++;
                }
            }
        }
        return counter;
    }

    /*
     * Swaps two items in a list. Mutates.
     * @param llist the list to swap in.
     * @param a, b - the indexes to swap.
     */
    private static void listSwap(List<SlidingTile> llist, int a, int b) {
        SlidingTile tmp = llist.get(a);
        llist.set(a, llist.get(b));
        llist.set(b, tmp);
    }

    /*
     * Changes the inversion polarity of the given list by swapping the first two elements.
     * Precondition: the list isn't solvable. Makes solvable puzzles unsolvable.
     * In addition, this only works since our algorithm ensures the blank tile isn't at the start.
     *
     * @param the list to "fix"
     */
    private static void makeSolvable(List<SlidingTile> llist) {
        listSwap(llist, 0, 1);
    }
}
