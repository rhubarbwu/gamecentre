package fall2018.csc2017.slidingtiles;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.IOException;

import fall2018.csc2017.ScoreboardActivity;
import fall2018.csc2017.db.Games;
import fall2018.csc2017.db.GamesaveController;
import fall2018.csc2017.db.User;
import fall2018.csc2017.R;

/**
 * The initial activity for the sliding puzzle tile game.
 */
public class SlidingTilesStartingActivity extends AppCompatActivity {
    private int boardDimension = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_slidingtiles_starting);
        addStartButtonListener();
        addLoadButtonListener();
        addScoreboardButtonListener();
    }

    /**
     * Allows the user to specify a new preset dimension.
     */
    public void onDimensionButtonsClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.Dimension3x3:
                if (checked) boardDimension = 3;
                break;
            case R.id.Dimension4x4:
                if (checked) boardDimension = 4;
                break;
            case R.id.Dimension5x5:
                if (checked) boardDimension = 5;
                break;
        }
    }

    /**
     * Activate the start button.
     */
    private void addStartButtonListener() {
        Button startButton = findViewById(R.id.StartButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToGame();
            }
        });
    }

    /**
     * Activate the load button.
     */
    private void addLoadButtonListener() {
        Button loadButton = findViewById(R.id.LoadButton);
        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    BoardManager loaded = loadSavedGame();

                    if (loaded != null) {
                        Toast.makeText(v.getContext(), "Loaded saved game", Toast.LENGTH_LONG).show();
                        switchToGame(loaded);
                    } else {
                        Toast.makeText(v.getContext(), "Could not find saved game, starting new game", Toast.LENGTH_LONG).show();
                        switchToGame();
                    }
                } catch (IOException | ClassNotFoundException e) {
                    Toast.makeText(v.getContext(), "Failed to load saved game", Toast.LENGTH_LONG).show();
                    switchToGame();
                }

                switchToGame();
            }
        });
    }

    /**
     * Activate the Scoreboard button.
     */
    private void addScoreboardButtonListener() {
        Button scoreboardButton = findViewById(R.id.ScoreboardButton);
        scoreboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    switchToScoreboard();
                } catch (Exception e) {
                    System.err.println(e);
                    Toast.makeText(v.getContext(), "Scoreboard Could Not Be Loaded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Switch to the GameActivity view to play the game.
     *
     * @param game the boardManager rep of a game
     */
    private void switchToGame(BoardManager game) {
        Intent tmp = new Intent(this, GameActivity.class);
        tmp.putExtra("boardManager", game);
        startActivity(tmp);
    }

    /**
     * Switch to the game activity to play a new game.
     */
    private void switchToGame() {
        switchToGame(new BoardManager(boardDimension, boardDimension));
    }

    /**
     * Switch to the ScoreboardActivity view to list the scores of SlidingTiles.
     */
    private void switchToScoreboard() {
        Intent tmp = new Intent(this, ScoreboardActivity.class);
        tmp.putExtra("gameId", Games.SLIDING_TILES.getValue());
        startActivity(tmp);
    }

    /**
     * Load a saved game from the database return it.
     *
     * @return the game save data, if it exists
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private BoardManager loadSavedGame() throws IOException, ClassNotFoundException {
        return (BoardManager) GamesaveController.getGamesaveData(User.getSignedInUser().getUsername(),
                Games.SLIDING_TILES.getValue());
    }
}
