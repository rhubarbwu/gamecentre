package fall2018.csc2017.slidingtiles;

import fall2018.csc2017.AbstractBoard;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * The sliding tiles slidingTilesBoard.
 */
public class SlidingTilesBoard extends AbstractBoard<SlidingTile> implements Iterable<SlidingTile> {

    /**
     * The default number of rows.
     */
    public final static int DEFAULT_NUM_ROWS = 4;

    /**
     * The default number of rows.
     */
    final static int DEFAULT_NUM_COLS = 4;

    /**
     * A new slidingTilesBoard of tiles in row-major order, in default dimensions.
     * Precondition: len(tiles) == DEFAULT_NUM_ROWS * DEFAULT_NUM_COLS
     *
     * @param tiles the tiles for the slidingTilesBoard
     */
    SlidingTilesBoard(List<SlidingTile> tiles) {
        this(tiles, DEFAULT_NUM_ROWS, DEFAULT_NUM_COLS);
    }

    /**
     * A new slidingTilesBoard of tiles in row-major order given the specified number of rows or columns.
     * Precondition: len(tiles) == num_rows * num_cols
     * Precondition: num_rows, num_cols > 0
     *
     * @param tiles the tiles for the slidingTilesBoard
     */
    SlidingTilesBoard(List<SlidingTile> tiles, int numRows, int numColumns) {
        super(SlidingTile.class, numRows, numColumns);
        Iterator<SlidingTile> iter = tiles.iterator();

        for (int row = 0; row != numRows; row++) {
            for (int col = 0; col != numColumns; col++) {
                this.tiles[row][col] = iter.next();
            }
        }
    }

    /**
     * Swap the tiles at (row1, col1) and (row2, col2)
     *
     * @param row1 the first tile row
     * @param col1 the first tile col
     * @param row2 the second tile row
     * @param col2 the second tile col
     */
    void swapTiles(int row1, int col1, int row2, int col2) {
        // Swap the two tiles
        SlidingTile tile1 = tiles[row1][col1];
        SlidingTile tile2 = tiles[row2][col2];
        tiles[row1][col1] = tile2;
        tiles[row2][col2] = tile1;

        setChanged();
        notifyObservers();
    }

    @Override
    public String toString() {
        return "SlidingTilesBoard{" +
                "tiles=" + Arrays.toString(tiles) +
                '}';
    }

    /**
     * Finds the blank tile adjacent to the tile on the slidingTilesBoard.
     *
     * @param position the position of the tile to move
     * @return the position of the empty empty tile adjacent to the tile that is being moved. If none is found, -1 is returned
     */
    public int getBlankTileLocation(int position) {
        int row = position / getNumRows();
        int col = position % getNumColumns();
        int blankId = getNumTiles();

        SlidingTile above = row == 0 ? null : getTile(row - 1, col);
        SlidingTile below = row == getNumRows() - 1 ? null : getTile(row + 1, col);
        SlidingTile left = col == 0 ? null : getTile(row, col - 1);
        SlidingTile right = col == getNumColumns() - 1 ? null : getTile(row, col + 1);

        int blankTileLocation = -1;
        if (above != null && above.getId() == blankId) { // If the blank tile is above
            blankTileLocation = ((row - 1) * getNumRows() + col);
        } else if (below != null && below.getId() == blankId) { // If the blank tile is below
            blankTileLocation = ((row + 1) * getNumRows() + col);
        } else if (left != null && left.getId() == blankId) { // If the blank tile is to the left
            blankTileLocation = (row * getNumRows() + (col - 1));
        } else if (right != null && right.getId() == blankId) { // If the blank tile is to the right
            blankTileLocation = (row * getNumRows() + (col + 1));
        }

        return blankTileLocation;
    }
}
