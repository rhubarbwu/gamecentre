package fall2018.csc2017.minesweeper;

import fall2018.csc2017.AbstractTile;
import fall2018.csc2017.R;

public class MinesweeperTile extends AbstractTile {
    /**
     * Constant for the hidden tile.
     */
    public static final int HIDDEN_TILE = R.drawable.minesweeper_tile_hidden;

    /**
     * Constant for the flagged tile.
     */
    public static final int FLAGGED_TILE = R.drawable.minesweeper_tile_flagged;

    /**
     * Constant for the not bomb tile.
     */
    public static final int NOT_BOMB_TILE = R.drawable.minesweeper_tile_not_bomb;

    /**
     * Constants for the numbered tiles and the bomb tile.
     */
    public static final int[] TILES = {
            R.drawable.minesweeper_tile_blank,
            R.drawable.minesweeper_tile_1,
            R.drawable.minesweeper_tile_2,
            R.drawable.minesweeper_tile_3,
            R.drawable.minesweeper_tile_4,
            R.drawable.minesweeper_tile_5,
            R.drawable.minesweeper_tile_6,
            R.drawable.minesweeper_tile_7,
            R.drawable.minesweeper_tile_8,
            R.drawable.minesweeper_tile_bomb
    };

    /**
     * The value of the tile.
     */
    private int tileVal;

    /**
     * If the tile is hidden.
     */
    private boolean hidden;

    /**
     * If the tile has been flagged.
     */
    private boolean flagged;

    /**
     * If the tile should be revealed (used for when the game ends).
     */
    private boolean revealed;

    /**
     * Creates a new tile with a value of 0 (blank tile).
     */
    public MinesweeperTile() {
        this(0);
    }

    /**
     * Creates a new tile with the value passed in.
     *
     * @param tileVal the value this tile should take
     */
    public MinesweeperTile(int tileVal) {
        this.tileVal = tileVal;
        this.hidden = true;
    }

    /**
     * Setter for the variable hidden.
     *
     * @param hidden new value for hidden.
     */
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * Getter for the variable hidden.
     *
     * @return true if the tile is hidden, false otherwise
     */
    public boolean isHidden() {
        return hidden;
    }

    /**
     * Setter for the value of the tile.
     *
     * @param tileVal the value of the tile
     */
    public void setTileVal(int tileVal) {
        this.tileVal = tileVal;
    }

    /**
     * Getter for the value of the tile.
     *
     * @return the value of the tile
     */
    public int getTileVal() {
        return tileVal;
    }

    /**
     * Setter for if the tile should be revealed.
     *
     * @param revealed true if the tile should be revealed, false otherwise
     */
    public void setRevealed(boolean revealed) {
        this.revealed = revealed;
    }

    /**
     * Return the background id that should be displayed.
     *
     * @return the background id
     */
    public int getBackground() {
        // If the tile should be revealed.
        if (revealed) {
            // If the tile is flagged.
            if (flagged) {
                // If the tile is a bomb, then return the flagged tile.
                if (tileVal == MinesweeperBoard.BOMB_VAL) {
                    return FLAGGED_TILE;
                } else { // Otherwise, the tile is not a bomb, then return the not bomb tile.
                    return NOT_BOMB_TILE;
                }
            } else { // Otherwise, this tile is not flagged.
                // If the tile is a bomb, then return the bomb tile.
                if (tileVal == MinesweeperBoard.BOMB_VAL) {
                    return TILES[MinesweeperBoard.BOMB_VAL];
                }
            }
        }

        // If the tile is flagged, the return the flagged tile.
        if (flagged)
            return FLAGGED_TILE;
        else if (hidden) // If the tile is hidden, the return the hidden tile.
            return HIDDEN_TILE;
        else // Otherwise, return the value of the tile.
            return TILES[tileVal];
    }

    /**
     * Getter for if this tile is flagged.
     *
     * @return true if this tile is flagged, false otherwise
     */
    public boolean isFlagged() {
        return flagged;
    }

    /**
     * Setter for if this tile is flagged.
     *
     * @param flagged if this tile is flagged.
     */
    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }
}
