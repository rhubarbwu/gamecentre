package fall2018.csc2017.minesweeper;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.IOException;

import fall2018.csc2017.ScoreboardActivity;
import fall2018.csc2017.db.Games;
import fall2018.csc2017.db.GamesaveController;
import fall2018.csc2017.db.User;
import fall2018.csc2017.R;

public class MinesweeperStartingActivity extends AppCompatActivity {
    private int boardRows = 8;
    private int boardColumns = 8;
    private int mines = 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_minesweeper_starting);

        addStartButtonListener();
        addLoadButtonListener();
        addScoreboardButtonListener();
    }

    /**
     * Listener for the dimension radio buttons.
     *
     * @param view
     */
    public void onDimensionButtonsClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        if (checked) {
            // Check which radio button was clicked
            switch (view.getId()) {
                case R.id.Dimension4x4:
                    boardRows = 4;
                    boardColumns = 4;
                    mines = 4;
                    break;
                case R.id.Dimension8x8:
                    boardRows = 8;
                    boardColumns = 8;
                    mines = 8;
                    break;
                case R.id.Dimension12x12:
                    boardRows = 12;
                    boardColumns = 12;
                    mines = 12;
                    break;
            }
        }
    }

    /**
     * Adds a listener for the start button.
     */
    private void addStartButtonListener() {
        Button startButton = findViewById(R.id.StartButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame();
            }
        });
    }

    /*
     * Loads the specified game.
     */
    private void startGame(BoardManager boardManager) {
        Intent tmp = new Intent(this, GameActivity.class);
        if (boardManager == null) {
            boardManager = new BoardManager(boardRows, boardColumns, mines);
        }
        tmp.putExtra("boardManager", boardManager);
        startActivity(tmp);
    }

    private void startGame() {
        startGame(new BoardManager(boardRows, boardColumns, mines));
    }

    /**
     * Activate the load button.
     */
    private void addLoadButtonListener() {
        Button loadButton = findViewById(R.id.LoadButton);
        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BoardManager boardManager = null;
                try {
                    boardManager = loadSavedGame();
                } catch (IOException | ClassNotFoundException e) {
                    Toast.makeText(v.getContext(), "Failed to load saved game", Toast.LENGTH_LONG).show();
                }

                if (boardManager == null) {
                    boardManager = new BoardManager(MinesweeperStartingActivity.this.boardRows,
                            MinesweeperStartingActivity.this.boardColumns,
                            MinesweeperStartingActivity.this.mines);
                    Toast.makeText(v.getContext(), "Could not find saved game, starting new game", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(v.getContext(), "Loaded saved game", Toast.LENGTH_LONG).show();
                }

                Intent intent = new Intent(MinesweeperStartingActivity.this, GameActivity.class);
                intent.putExtra("boardManager", boardManager);
                MinesweeperStartingActivity.this.startActivity(intent);
            }
        });
    }

    /**
     * Activate the Scoreboard button.
     */
    private void addScoreboardButtonListener() {
        Button scoreboardButton = findViewById(R.id.ScoreboardButton);
        scoreboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    switchToScoreboard();
                } catch (Exception e) {
                    System.err.println(e);
                    Toast.makeText(v.getContext(), "Scoreboard Could Not Be Loaded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Switch to the ScoreboardActivity view to list the scores of SlidingTiles.
     */
    private void switchToScoreboard() {
        Intent tmp = new Intent(this, ScoreboardActivity.class);
        tmp.putExtra("gameId", Games.MINESWEEPER.getValue());
        startActivity(tmp);
    }

    /**
     * Load a saved game from the database and set it to the current BoardManager.
     *
     * @return the BoardManager instance if a save was found in the database, null otherwise
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private BoardManager loadSavedGame() throws ClassNotFoundException, IOException {
        return (BoardManager) GamesaveController.getGamesaveData(User.getSignedInUser().getUsername(),
                Games.MINESWEEPER.getValue());
    }
}