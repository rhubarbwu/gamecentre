package fall2018.csc2017.minesweeper;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import fall2018.csc2017.AbstractBoard;

public class MinesweeperBoard extends AbstractBoard<MinesweeperTile> implements Iterable<MinesweeperTile> {
    /**
     * Constant for the value of a bomb.
     */
    public static final int BOMB_VAL = 9;

    /**
     * Default dimensions.
     */
    public static final int DEFAULT_DIMENSION = 8;

    /**
     * Initializes the minesweeperBoard with empty tiles.
     *
     * @param numRows    the number of rows this minesweeperBoard has
     * @param numColumns the number of columns this minesweeperBoard has
     */
    MinesweeperBoard(int numRows, int numColumns) {
        super(MinesweeperTile.class, numRows, numColumns);

        // Initialize the MinesweeperBoard with empty tiles.
        for (int row = 0; row < numRows; row++) {
            for (int column = 0; column < numColumns; column++) {
                this.tiles[row][column] = new MinesweeperTile();
            }
        }
    }

    public MinesweeperBoard() {
        this(DEFAULT_DIMENSION, DEFAULT_DIMENSION);
    }

    /**
     * Initializes the MinesweeperBoard with predefined tiles.
     *
     * @param numRows    the number of rows this minesweeperBoard has
     * @param numColumns the number of columns this minesweeperBoard has
     */
    MinesweeperBoard(List<MinesweeperTile> tiles, int numRows, int numColumns) {
        super(MinesweeperTile.class, numRows, numColumns);
        Iterator<MinesweeperTile> iter = tiles.iterator();

        // Initialize the minesweeperBoard with tiles
        for (int row = 0; row < numRows; row++) {
            for (int column = 0; column < numColumns; column++) {
                this.tiles[row][column] = iter.next();
            }
        }
    }

    @Override
    public String toString() {
        return "SlidingTilesBoard{" +
                "tiles=" + Arrays.toString(tiles) +
                '}';
    }


}
