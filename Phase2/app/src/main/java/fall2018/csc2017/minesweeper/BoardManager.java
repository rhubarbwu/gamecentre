package fall2018.csc2017.minesweeper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Set;

public class BoardManager extends Observable implements Serializable {
    /**
     * The MinesweeperBoard.
     */
    private MinesweeperBoard minesweeperBoard;

    /**
     * The number of bombs in the board.
     */
    private int numBombs;

    /**
     * Number of tiles on the board marked as bomb.
     */
    private int numBombsMarked;

    /**
     * True if the board has been initialized, false otherwise.
     */
    private boolean initialized;

    /**
     * True if the game is over (either solved or the player has lost), false otherwise.
     */
    private boolean gameOver;

    /**
     * Number of seconds spent on this game.
     */
    private long gameTime;

    /**
     * Number of clicks made.
     */
    private int clicks;

    /**
     * Constructor that initializes the minesweeperBoard.
     *
     * @param numRows    the number of rows this minesweeperBoard has
     * @param numColumns the number of columns this minesweeperBoard has
     * @param numBombs   the number of bombs this minesweeperBoard has
     */
    BoardManager(int numRows, int numColumns, int numBombs) {
        this(new MinesweeperBoard(numRows, numColumns), numBombs, false);
    }

    BoardManager(MinesweeperBoard minesweeperBoard, int numBombs, boolean initialized) {
        this.minesweeperBoard = minesweeperBoard;
        this.numBombs = numBombs;
        this.numBombsMarked = 0;
        this.initialized = initialized;
        this.gameOver = false;
        this.gameTime = 0;
        this.clicks = 0;
    }

    public BoardManager() {
        this(new MinesweeperBoard(), 0, false);
    }

    /**
     * Getter for the minesweeperBoard.
     *
     * @return the minesweeperBoard
     */
    public MinesweeperBoard getMinesweeperBoard() {
        return minesweeperBoard;
    }

    /**
     * Getter for the time spent on this game.
     *
     * @return the number of milliseconds spent on this game
     */
    public long getGameTime() {
        return gameTime;
    }

    /**
     * Setter for the time spent on this game.
     *
     * @param gameTime the number of milliseconds spent on this game
     */
    public void setGameTime(long gameTime) {
        this.gameTime = gameTime;
    }

    /**
     * Getter for the number of bombs that have been flagged as bomb.
     *
     * @return the number of bombs marked as bomb
     */
    public int getNumBombsMarked() {
        return numBombsMarked;
    }

    /**
     * Getter for the number of bombs in the board.
     *
     * @return the number of bombs in the board
     */
    public int getNumBombs() {
        return numBombs;
    }

    /**
     * Computes the score. Score is computed to be board size - number of clicks.
     *
     * @return the score
     */
    public int getScore() {
        return minesweeperBoard.getNumColumns() * minesweeperBoard.getNumRows() - clicks;
    }

    /**
     * Reveals a tile on the minesweeperBoard.
     *
     * @param position the position of the tile to reveal
     */
    public void revealTile(int position) {
        // If the game is gameOver, then do not reveal anything.
        if (gameOver)
            return;

        // If the game is not initialized, the generate the minesweeperBoard.
        if (!initialized) {
            generateGame(position);

            initialized = true;
        }

        int row = position / minesweeperBoard.getNumRows();
        int column = position % minesweeperBoard.getNumColumns();

        // Get the tile that is to be revealed.
        MinesweeperTile tappedTile = minesweeperBoard.getTile(row, column);

        // Only reveal the tapped tile if it is not flagged as a bomb.
        if (!tappedTile.isFlagged()) {
            unhideAdjacentBlankTiles(row, column);

            // If the tile tapped is a bomb, then reveal all bombs.
            if (tappedTile.getTileVal() == MinesweeperBoard.BOMB_VAL) {
                gameOver = true;
                revealBombLocations();
            }
        }

        clicks++;

        setChanged();
        notifyObservers();
    }

    /**
     * Mark a tile on the minesweeperBoard as a bomb.
     *
     * @param position the position of the tile to mark as a bomb
     * @return true if a bomb was marked, false otherwise
     */
    public boolean markAsBomb(int position) {
        // If the game is gameOver, then do not mark anything.
        if (gameOver)
            return false;

        int row = position / minesweeperBoard.getNumRows();
        int column = position % minesweeperBoard.getNumColumns();

        // Get the tile that is to be marked.
        MinesweeperTile tappedTile = minesweeperBoard.getTile(row, column);

        // Mark the tile only if it is still hidden.
        if (tappedTile.isHidden()) {
            // If the tile is already flagged, then unflag the tile.
            if (tappedTile.isFlagged()) {
                tappedTile.setFlagged(false);
                numBombsMarked -= 1;
            } else {
                // If no more bombs can be flagged, return false.
                if (!canMarkMoreBombs())
                    return false;

                // Otherwise the tile is not flagged, so flag the tile.
                tappedTile.setFlagged(true);
                numBombsMarked += 1;
            }
        }

        // Compute if the minesweeperBoard is now gameOver.
        gameOver = computeSolved();

        setChanged();
        notifyObservers();

        return true;
    }

    /**
     * Returns if more bombs can be marked.
     *
     * @return true if more bombs can be marked, false otherwise
     */
    public boolean canMarkMoreBombs() {
        return numBombsMarked < numBombs;
    }

    /**
     * Getter for if the minesweeperBoard is gameOver.
     *
     * @return true if the minesweeperBoard is gameOver, false otherwise
     */
    public boolean isGameOver() {
        return gameOver;
    }

    /**
     * Recursively unhides adjacent blank tiles and non-blank tiles (that are not bombs) for up to a depth of one.
     *
     * @param row    the row of the tile to start at
     * @param column the column of the tile to start at
     */
    private void unhideAdjacentBlankTiles(int row, int column) {
        // If the coordinates specify an invalid location, return.
        if (row < 0 || row >= minesweeperBoard.getNumRows() || column < 0 || column >= minesweeperBoard.getNumColumns())
            return;

        // Get the tile from the coordinate.
        MinesweeperTile tile = minesweeperBoard.getTile(row, column);

        // If the tile is already not hidden, then don't unhide it.
        if (!tile.isHidden())
            return;

        // Unhide the tile
        tile.setHidden(false);

        // If the current tile is a blank tile, then recursively unhide adjacent blank tiles.
        if (tile.getTileVal() == 0) {
            unhideAdjacentBlankTiles(row + 1, column);
            unhideAdjacentBlankTiles(row, column + 1);
            unhideAdjacentBlankTiles(row - 1, column);
            unhideAdjacentBlankTiles(row, column - 1);
        }
    }

    /**
     * Generates the game with a safe location (i.e. that location will not contain the bomb).
     *
     * @param safeLoc the location on the minesweeperBoard that cannot contain the bomb
     */
    private void generateGame(int safeLoc) {
        // Set to store the generated bomb locations.
        Set<Integer> bombLoc = generateBombLocations(minesweeperBoard.getNumTiles(), safeLoc);

        // Build the temporary minesweeperBoard.
        int[][] tempBoard = new int[minesweeperBoard.getNumRows()][minesweeperBoard.getNumColumns()];
        for (int loc : bombLoc) {
            int bombRow = loc / minesweeperBoard.getNumRows();
            int bombColumn = loc % minesweeperBoard.getNumColumns();

            tempBoard[bombRow][bombColumn] = MinesweeperBoard.BOMB_VAL;
            // Update the values of the adjacent pieces to the bomb.
            updateAdjacentPiecesToBomb(tempBoard, bombRow, bombColumn);
        }

        // Create the tiles from the temporary minesweeperBoard that was built.
        for (int row = 0; row < minesweeperBoard.getNumRows(); row++) {
            for (int column = 0; column < minesweeperBoard.getNumColumns(); column++) {
                MinesweeperTile tile = minesweeperBoard.getTile(row, column);
                tile.setTileVal(tempBoard[row][column]);
            }
        }
    }

    /**
     * Updates the values of adjacent pieces to the bomb on the minesweeperBoard.
     *
     * @param board      the minesweeperBoard to be updated.
     * @param bombRow    the row of the bomb.
     * @param bombColumn the column of the bomb.
     */
    private void updateAdjacentPiecesToBomb(int[][] board, int bombRow, int bombColumn) {
        // Loop through every piece adjacent to the bomb and update its value.
        for (int rowAdjust = -1; rowAdjust < 2; rowAdjust++) {
            for (int columnAdjust = -1; columnAdjust < 2; columnAdjust++) {
                int tempRow = bombRow + rowAdjust;
                int tempColumn = bombColumn + columnAdjust;

                if (tempRow >= 0 && tempRow < this.minesweeperBoard.getNumRows()) {
                    if (tempColumn >= 0 && tempColumn < this.minesweeperBoard.getNumColumns()) {
                        // Only update if the piece to be updated is not a bomb itself.
                        if (board[tempRow][tempColumn] != MinesweeperBoard.BOMB_VAL) {
                            board[tempRow][tempColumn] += 1;
                        }
                    }
                }
            }
        }
    }

    /**
     * Generate locations for the bombs with a safe location.
     *
     * @param boardSize the size of the minesweeperBoard
     * @param safeLoc   the safe location
     * @return the locations of the bombs
     */
    private Set<Integer> generateBombLocations(int boardSize, int safeLoc) {
        Set<Integer> bombLoc = new HashSet<>();

        List<Integer> locations = new ArrayList<>();
        for (int i = 0; i < boardSize; i++) {
            if (i != safeLoc) {
                locations.add(i);
            }
        }
        Collections.shuffle(locations);

        for (int i = 0; i < numBombs; i++) {
            bombLoc.add(locations.get(i));
        }

        return bombLoc;
    }

    /**
     * Reveals all the pieces (specifically the bombs).
     */
    private void revealBombLocations() {
        for (MinesweeperTile t : minesweeperBoard) {
            t.setRevealed(true);
        }
    }

    /**
     * Compute if the minesweeperBoard has been gameOver.
     *
     * @return true if the minesweeperBoard has been gameOver, false otherwise
     */
    public boolean computeSolved() {
        // If not all flags have been used, then the minesweeperBoard cannot be gameOver.
        if (canMarkMoreBombs())
            return false;

        for (MinesweeperTile t : minesweeperBoard) {
            // If the tile is flagged, but is not a bomb, then the minesweeperBoard is not gameOver.
            if (t.isFlagged() && t.getTileVal() != MinesweeperBoard.BOMB_VAL) {
                return false;
            }
        }

        // Otherwise, the minesweeperBoard is gameOver.
        return true;
    }
}
