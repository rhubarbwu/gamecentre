package fall2018.csc2017.pipes;

import java.io.Serializable;

import fall2018.csc2017.AbstractTile;
import fall2018.csc2017.R;

/**
 * A MinesweeperTile in a sliding tiles puzzle.
 */
public class PipeTile extends AbstractTile implements Serializable {

    /**
     * Enums for types of PipeTiles
     */
    private static final int EMPTYTILE = 0;
    private static final int STARTTILE = 1;
    private static final int CROSS = 2;
    private static final int STRAIGHT = 3;
    private static final int TURN = 4;
    private static final int NAILHEAD = 5;

    /**
     * Enums for default rotations for rotatable pipes.
     */
    private static final String[] DEFAULT_CONNECTIONS = {
            "0000", "1010", "1111", "1010", "1100", "1101"
    };

    /**
     * Enums for empty tile image.
     */
    private static final int[] EMPTYIMAGES = {
            R.drawable.pipes_empty,
            R.drawable.pipes_empty,
            R.drawable.pipes_empty,
            R.drawable.pipes_empty
    };

    /**
     * Enums for orientations and images for the start tile.
     */
    private static final int[] STARTIMAGES = {
            R.drawable.pipes_hor_startpipe,
            R.drawable.pipes_ver_startpipe,
            R.drawable.pipes_hor_startpipe,
            R.drawable.pipes_ver_startpipe
    };

    /**
     * Enums for the cross tile.
     */
    private static final int[] CROSSIMAGES = {
            R.drawable.pipes_awsd_crosspipe,
            R.drawable.pipes_awsd_crosspipe,
            R.drawable.pipes_awsd_crosspipe,
            R.drawable.pipes_awsd_crosspipe
    };

    /**
     * Enums for orientations and images for the straight tile.
     */
    private static final int[] STRAIGHTIMAGES = {
            R.drawable.pipes_hor_straightpipe,
            R.drawable.pipes_ver_straightpipe,
            R.drawable.pipes_hor_straightpipe,
            R.drawable.pipes_ver_straightpipe
    };

    /**
     * Enums for orientations and images for the turn tile.
     */
    private static final int[] TURNIMAGES = {
            R.drawable.pipes_upleft_turnpipe,
            R.drawable.pipes_upright_turnpipe,
            R.drawable.pipes_downright_turnpipe,
            R.drawable.pipes_downleft_turnpipe,
    };

    /**
     * Enums for orientations and images for the nailhead tile.
     */
    private static final int[] NAILHEADIMAGES = {
            R.drawable.pipes_left_nailpipe,
            R.drawable.pipes_up_nailpipe,
            R.drawable.pipes_right_nailpipe,
            R.drawable.pipes_down_nailpipe,
    };

    /**
     * Enum Array collection for PipeTile image orientation sets.
     */
    private static final int[][] PIPEIMAGES = {
            EMPTYIMAGES, STARTIMAGES, CROSSIMAGES, STRAIGHTIMAGES, TURNIMAGES, NAILHEADIMAGES
    };

    /**
     * Instance variable integer for what type of PipeTile this is.
     */
    private int type;

    /**
     * Instance variable integer for what orientation PipeTile is in.
     * It is an integer between 0-3 representing the four possible orientations.
     * This variable will not have a value for non-rotatable PipeTile tiles.
     */
    private int orientation;

    /**
     * Instance variable boolean for whether or not the tile is rotatable.
     */
    private boolean rotatable;

    /**
     * Instance variable String for which connections the PipeTile has.
     * By definition, we list them as left, up, right, down.
     * For example, 0101 would be a straight tile connecting up and down.
     */
    private String connections;

    /**
     * Getter method for whether or not this PipeTile is turnable.
     *
     * @return rotatable     a boolean of whether or not the tile is rotatable.
     */
    public boolean isRotatable() {
        return this.rotatable;
    }

    /**
     * Getter method for the tiles connections.
     *
     * @return connections     a string representation of which directions the tile is connected to.
     */
    public String getConnections() {
        return this.connections;
    }

    /**
     * Method to rotate a tile (assuming it's rotatable)
     *
     * @param isUndo a boolean for whether or not this is an undo move.
     */
    public void rotate(boolean isUndo) {
        if (isUndo) {
            this.orientation = (this.orientation + 3) % 4;
            this.connections = this.connections.substring(1, 4) + this.connections.substring(0, 1);
        } else {
            this.orientation = (this.orientation + 1) % 4;
            this.connections = this.connections.substring(3, 4) + this.connections.substring(0, 3);
        }
        this.background = PIPEIMAGES[this.type][this.orientation];
    }

    /**
     * A PipeTile with a type and orientation
     *
     * @param type        the id of the type of the tile
     * @param orientation the id of the direction of the tile
     */
    public PipeTile(int type, int orientation) {
        this.type = type;
        this.rotatable = (type != EMPTYTILE && type != STARTTILE && type != CROSS);
        if (this.rotatable) orientation = (int) (Math.random() * 4);
        this.connections = DEFAULT_CONNECTIONS[this.type];
        for (int i = 0; i < orientation; i++) this.rotate(false);
        this.background = PIPEIMAGES[this.type][this.orientation];
    }

    /**
     * Returns the type of PipeTile
     *
     * @return type         the type of this PipeTile
     */
    public int getType() {
        return this.type;
    }
}
