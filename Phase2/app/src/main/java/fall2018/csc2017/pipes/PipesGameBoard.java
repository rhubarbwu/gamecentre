package fall2018.csc2017.pipes;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import fall2018.csc2017.AbstractBoard;

/**
 * The gameboard PipesGameBoard for Pipes.
 */
public class PipesGameBoard extends AbstractBoard<PipeTile> implements Iterable<PipeTile> {

    /**
     * The number of player-rotatable tiles on this board
     */
    private int numOfPlayable;

    /**
     * The default number of rows.
     */
    public static final int DEFAULTLEVEL = 1;

    /**
     * Level Data for levels 0-9. Level 0 is a testing dud (for unit tests).
     */
    public static final int[][] LEVEL_0 = {
            {0, 0}, {0, 0}, {0, 0},
            {1, 0}, {4, 0}, {1, 0},
            {0, 0}, {0, 0}, {0, 0}
    };

    public static final int[][] LEVEL_1 = {
            {0, 0}, {0, 0}, {0, 0},
            {1, 0}, {4, 0}, {0, 0},
            {0, 0}, {1, 1}, {0, 0}
    };

    public static final int[][] LEVEL_2 = {
            {0, 0}, {0, 0}, {1, 1}, {0, 0},
            {1, 0}, {5, 1}, {5, 2}, {0, 0},
            {0, 0}, {4, 0}, {4, 2}, {0, 0},
            {0, 0}, {0, 0}, {0, 0}, {0, 0}
    };

    public static final int[][] LEVEL_3 = {
            {0, 0}, {1, 1}, {1, 1}, {0, 0}, {0, 0},
            {0, 0}, {3, 0}, {4, 4}, {5, 0}, {1, 0},
            {0, 0}, {4, 4}, {5, 0}, {4, 0}, {0, 0},
            {0, 0}, {4, 0}, {4, 0}, {3, 0}, {1, 0},
            {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}
    };

    public static final int[][] LEVEL_4 = {
            {0, 0}, {0, 0}, {1, 1}, {0, 0}, {0, 0}, {0, 0},
            {0, 0}, {3, 3}, {4, 0}, {4, 0}, {4, 1}, {0, 0},
            {0, 0}, {4, 0}, {3, 1}, {2, 0}, {3, 3}, {1, 0},
            {0, 0}, {3, 1}, {4, 1}, {5, 1}, {4, 1}, {0, 0},
            {1, 0}, {4, 1}, {4, 0}, {5, 3}, {4, 2}, {0, 0},
            {0, 0}, {0, 0}, {0, 0}, {0, 0}, {1, 1}, {0, 0}
    };

    public static final int[][] LEVEL_5 = {
            {0, 0}, {1, 1}, {0, 0}, {1, 1}, {0, 0}, {0, 0}, {0, 0},
            {0, 0}, {4, 0}, {4, 3}, {4, 2}, {5, 0}, {5, 1}, {1, 0},
            {1, 0}, {3, 1}, {5, 1}, {5, 2}, {4, 1}, {3, 0}, {0, 0},
            {1, 0}, {3, 0}, {5, 2}, {2, 0}, {5, 3}, {5, 2}, {0, 0},
            {0, 0}, {4, 1}, {4, 3}, {5, 0}, {4, 0}, {5, 1}, {1, 0},
            {0, 0}, {4, 3}, {3, 0}, {4, 0}, {4, 1}, {4, 3}, {0, 0},
            {0, 0}, {0, 0}, {0, 0}, {0, 0}, {1, 1}, {0, 0}, {0, 0}
    };

    public static final int[][] LEVEL_6 = {
            {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {1, 1}, {0, 0}, {0, 0},
            {0, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {1, 0},
            {0, 0}, {4, 0}, {3, 0}, {3, 0}, {5, 0}, {4, 0}, {4, 0}, {0, 0},
            {1, 0}, {4, 0}, {3, 0}, {4, 0}, {4, 0}, {2, 0}, {4, 0}, {0, 0},
            {1, 0}, {4, 0}, {3, 0}, {4, 0}, {4, 0}, {3, 0}, {4, 0}, {1, 0},
            {0, 0}, {4, 0}, {3, 0}, {3, 0}, {3, 0}, {5, 0}, {4, 0}, {0, 0},
            {0, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {0, 0},
            {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {1, 1}, {0, 0}
    };

    public static final int[][] LEVEL_7 = {
            {0, 0}, {1, 1}, {0, 0}, {0, 0}, {1, 1}, {0, 0}, {0, 0}, {1, 1}, {0, 0},
            {1, 0}, {5, 0}, {4, 0}, {4, 0}, {2, 0}, {4, 0}, {4, 0}, {5, 0}, {1, 0},
            {0, 0}, {4, 0}, {4, 0}, {2, 0}, {5, 0}, {2, 0}, {4, 0}, {4, 0}, {0, 0},
            {0, 0}, {4, 0}, {5, 0}, {4, 0}, {4, 0}, {4, 0}, {5, 0}, {4, 0}, {0, 0},
            {0, 0}, {5, 0}, {2, 0}, {3, 0}, {3, 0}, {3, 0}, {2, 0}, {5, 0}, {0, 0},
            {0, 0}, {4, 0}, {5, 0}, {4, 0}, {4, 0}, {4, 0}, {5, 0}, {4, 0}, {0, 0},
            {0, 0}, {4, 0}, {4, 0}, {2, 0}, {5, 0}, {2, 0}, {4, 0}, {4, 0}, {0, 0},
            {1, 0}, {5, 0}, {4, 0}, {4, 0}, {2, 0}, {4, 0}, {4, 0}, {5, 0}, {1, 0},
            {0, 0}, {1, 1}, {0, 0}, {0, 0}, {1, 1}, {0, 0}, {0, 0}, {1, 1}, {0, 0}
    };

    public static final int[][] LEVEL_8 = {
            {0, 0}, {1, 1}, {0, 0}, {1, 1}, {0, 0}, {1, 1}, {0, 0}, {0, 0}, {1, 1}, {0, 0},
            {1, 0}, {5, 0}, {3, 0}, {5, 0}, {4, 0}, {4, 0}, {4, 0}, {5, 0}, {4, 0}, {0, 0},
            {0, 0}, {3, 0}, {5, 0}, {3, 0}, {3, 0}, {3, 0}, {5, 0}, {4, 0}, {3, 0}, {0, 0},
            {0, 0}, {4, 0}, {3, 0}, {4, 0}, {5, 0}, {4, 0}, {3, 0}, {5, 0}, {4, 0}, {1, 0},
            {1, 0}, {2, 0}, {3, 0}, {5, 0}, {2, 0}, {4, 0}, {3, 0}, {4, 0}, {4, 0}, {0, 0},
            {0, 0}, {4, 0}, {4, 0}, {3, 0}, {4, 0}, {4, 0}, {4, 0}, {5, 0}, {3, 0}, {0, 0},
            {1, 0}, {5, 0}, {4, 0}, {4, 0}, {3, 0}, {4, 0}, {4, 0}, {5, 0}, {4, 0}, {0, 0},
            {0, 0}, {5, 0}, {5, 0}, {3, 0}, {4, 0}, {4, 0}, {4, 0}, {5, 0}, {4, 0}, {0, 0},
            {0, 0}, {5, 0}, {4, 0}, {4, 0}, {5, 0}, {4, 0}, {4, 0}, {5, 0}, {4, 0}, {0, 0},
            {0, 0}, {1, 1}, {0, 0}, {0, 0}, {1, 1}, {1, 1}, {0, 0}, {1, 1}, {0, 0}, {0, 0}
    };

    public static final int[][] LEVEL_9 = {
            {0, 0}, {1, 1}, {1, 1}, {0, 0}, {0, 0}, {1, 1}, {0, 0}, {0, 0}, {0, 0}, {1, 1}, {0, 0},
            {1, 0}, {5, 0}, {4, 0}, {5, 0}, {3, 0}, {2, 0}, {4, 0}, {5, 0}, {4, 0}, {4, 0}, {0, 0},
            {0, 0}, {4, 0}, {5, 0}, {5, 0}, {4, 0}, {5, 0}, {4, 0}, {4, 0}, {3, 0}, {5, 0}, {0, 0},
            {0, 0}, {3, 0}, {3, 0}, {5, 0}, {4, 0}, {3, 0}, {4, 0}, {3, 0}, {3, 0}, {4, 0}, {0, 0},
            {0, 0}, {4, 0}, {3, 0}, {3, 0}, {3, 0}, {5, 0}, {4, 0}, {4, 0}, {2, 0}, {5, 0}, {1, 0},
            {0, 0}, {5, 0}, {5, 0}, {4, 0}, {3, 0}, {4, 0}, {5, 0}, {3, 0}, {3, 0}, {4, 0}, {0, 0},
            {0, 0}, {4, 0}, {4, 0}, {3, 0}, {5, 0}, {4, 0}, {5, 0}, {4, 0}, {3, 0}, {5, 0}, {0, 0},
            {0, 0}, {5, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {4, 0}, {0, 0},
            {1, 0}, {4, 0}, {4, 0}, {5, 0}, {3, 0}, {3, 0}, {4, 0}, {4, 0}, {5, 0}, {5, 0}, {1, 0},
            {0, 0}, {3, 0}, {4, 0}, {4, 0}, {4, 0}, {5, 0}, {4, 0}, {2, 0}, {5, 0}, {4, 0}, {0, 0},
            {0, 0}, {0, 0}, {1, 1}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {1, 1}, {1, 1}, {0, 0}, {0, 0}
    };

    public static final int[][][] LEVELS = {
            LEVEL_0,
            LEVEL_1,
            LEVEL_2,
            LEVEL_3,
            LEVEL_4,
            LEVEL_5,
            LEVEL_6,
            LEVEL_7,
            LEVEL_8,
            LEVEL_9
    };

    /**
     * A new PipesGameBoard of tiles in row-major order given the specified number of rows or columns.
     * Precondition: 0 <= level <= 9
     * Precondition: tiles is a valid List<PipeTile> with a perfect square length.
     *
     * @param pipeTiles the List of PipeTiles
     * @param level     the level of this board
     */
    PipesGameBoard(List<PipeTile> pipeTiles, int level) {
        super(PipeTile.class, Math.max(3, level + 2), Math.max(3, level + 2));
        Iterator<PipeTile> iter = pipeTiles.iterator();
        this.numOfPlayable = 0;

        for (int row = 0; row != Math.max(3, level + 2); row++) {
            for (int col = 0; col != Math.max(3, level + 2); col++) {
                this.tiles[row][col] = iter.next();
                if (tiles[row][col].getType() > 2) this.numOfPlayable++;
            }
        }
    }

    /**
     * Rotate the pipeTile at (row, col) assuming it's supposed to be rotated.
     *
     * @param position  the position of the PipeTile in the PipesGameBoard we want to rotate.
     * @param isUndo    whether or not this rotation is the result of an undo (would change the
     *                  direction of rotation).
     */
    void rotateTile(int position, boolean isUndo) {
        PipeTile pipeTile = this.getTile(position);
        pipeTile.rotate(isUndo);
        setChanged();
        notifyObservers();
    }

    /**
     * Given the index of some PipeTile in this PipesGameBoard, return an array containing all
     * connected pipes to PipeTile in any path previously traversed.
     *
     * @param thisPipeIndex  the index of this PipeTile
     * @param pipesTravelled the HashSet of PipeTiles already traversed
     */
    HashSet<Integer> getConnectedPipes(int thisPipeIndex, HashSet<Integer> pipesTravelled) {
        int row = thisPipeIndex / numRows;
        int col = thisPipeIndex % numColumns;
        // we could use thisPipeIndex, but we need row and col for later logic, so we use it here
        PipeTile thisPipe = this.getTile(row, col);

        /*  if this tile has not previously been traversed,
            add it to the set, and recursively traverse each of the connected PipeTiles
        */
        if (!pipesTravelled.contains(thisPipeIndex)) {
            pipesTravelled.add(thisPipeIndex); // add the thisPipeIndex to the set

            if (col > 0) { // check the left neighbour if it exists
                PipeTile leftTile = this.getTile(row, col - 1);
                /*  if the left neighbour Pipe is connected correctly, use recursion to find the
                    paths of the left Pipe*/
                if (leftTile.getConnections().substring(2, 3).equals("1") &&
                        thisPipe.getConnections().substring(0, 1).equals("1")) {
                    int leftIndex = row * numRows + (col - 1);
                    pipesTravelled.addAll(getConnectedPipes(leftIndex, pipesTravelled));
                }
            }
            if (row > 0) { // check the above neighbour if it exists
                PipeTile upTile = this.getTile(row - 1, col);
                /*  if the up neighbour Pipe is connected correctly, use recursion to find the
                    paths of the up Pipe*/
                if (upTile.getConnections().substring(3, 4).equals("1") &&
                        thisPipe.getConnections().substring(1, 2).equals("1")) {
                    int upIndex = (row - 1) * numRows + col;
                    pipesTravelled.addAll(getConnectedPipes(upIndex, pipesTravelled));
                }
            }
            if (col < numColumns - 1) { // check the right neighbour if it exists
                PipeTile rightTile = this.getTile(row, col + 1);
                /*  if the right neighbour Pipe is connected correctly, use recursion to find the
                    paths of the right Pipe*/
                if (rightTile.getConnections().substring(0, 1).equals("1") &&
                        thisPipe.getConnections().substring(2, 3).equals("1")) {
                    int rightIndex = row * numRows + (col + 1);
                    pipesTravelled.addAll(getConnectedPipes(rightIndex, pipesTravelled));
                }
            }
            if (row < numRows - 1) { // check the below neighbour if it exists
                PipeTile downTile = this.getTile(row + 1, col);
                /*  if the down neighbour Pipe is connected correctly, use recursion to find the
                    paths of the down Pipe*/
                if (downTile.getConnections().substring(1, 2).equals("1") &&
                        thisPipe.getConnections().substring(3, 4).equals("1")) {
                    int downIndex = (row + 1) * numRows + (col);
                    pipesTravelled.addAll(getConnectedPipes(downIndex, pipesTravelled));
                }
            }
        }
        return pipesTravelled; // the set of PipeTiles that have been traversed
    }

    /**
     * Given the index of a PipeTile in this PipesGameBoard, determine if this PipeTile leaks on
     * any side. That is, if on any side, this PipeTile is open but that neighbouring PipeTile is
     * closed on the side facing this PipeTile(causing water to leak out of the circuit).
     *
     * @param thisPipeIndex the index of this PipeTile
     * @return whether or not this PipeTile leaks
     */
    boolean doesLeak(int thisPipeIndex) {
        boolean doesLeak = false;
        int row = thisPipeIndex / numRows;
        int col = thisPipeIndex % numColumns;
        String thisPipeConnections = this.getTile(thisPipeIndex).getConnections();
        int thisPipeType = this.getTile(thisPipeIndex).getType();

        if (thisPipeType > 2) { // only check the connections if it's a playable PipeTile
            if (col > 0) {
                PipeTile leftTile = this.getTile(row, col - 1);
                /*  There is a leak for this PipeTile iff the this PipeTile is open on the left
                    side, but the left neighbour is not open on the right. (The inverse only
                    matters if the left neighbour is also in the circuit, which will be checked
                    on its own.) This boolean formula uses the arrow law of implications.
                */
                doesLeak = doesLeak || (leftTile.getConnections().substring(2, 3).equals("0") && thisPipeConnections.substring(0, 1).equals("1"));
            }
            if (row > 0) {
                PipeTile upTile = this.getTile(row - 1, col);
                /*  There is a leak for this PipeTile iff the this PipeTile is open on the up
                    side, but the above neighbour is not open on the down side. (The inverse only
                    matters if the above neighbour is also in the circuit, which will be checked
                    on its own.) This boolean formula uses the arrow law of implications.
                */
                doesLeak = doesLeak || (upTile.getConnections().substring(3, 4).equals("0")
                        && thisPipeConnections.substring(1, 2).equals("1"));
            }
            if (col < numColumns - 1) {
                PipeTile rightTile = this.getTile(row, col + 1);
                /*  There is a leak for this PipeTile iff the this PipeTile is open on the right
                    side, but the right neighbour is not open on the left side. (The inverse only
                    matters if the right neighbour is also in the circuit, which will be checked
                    on its own.) This boolean formula uses the arrow law of implications.
                */
                doesLeak = doesLeak || (rightTile.getConnections().substring(0, 1).equals("0")
                        && thisPipeConnections.substring(2, 3).equals("1"));
            }
            if (row < numRows - 1) {
                PipeTile downTile = this.getTile(row + 1, col);

                /*  There is a leak for this PipeTile iff the this PipeTile is open on the down
                    side, but the below neighbour is not open on the up side. (The inverse only
                    matters if the below neighbour is also in the circuit, which will be checked
                    on its own.) This boolean formula uses the arrow law of implications.
                */
                if (downTile.getConnections().substring(1, 2).equals("0") && thisPipeConnections.substring(3, 4).equals("1"))
                    return true;
            }
        }
        return false; // if the method makes it this far, this PipeTile does not leak
    }

    /**
     * Given the index in the board of a desired PipeTile, return the PipeTile.
     * This method uses overloading to use the row and column represented by <index>
     *
     * @param index the index in the board of the desired PipeTile
     * @return the PipeTile in the index position of the board's row-major order
     */
    public PipeTile getTile(int index) {
        int row = index / numRows, col = index % numColumns;
        return getTile(row, col);
    }

    /**
     * Return the number of player-rotatable PipeTiles on this PipesGameBoard
     *
     * @return the number of player-rotatable PipeTiles on this PipesGameBoard
     */
    public int getNumOfPlayable() {
        return this.numOfPlayable;
    }
}
