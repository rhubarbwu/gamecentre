package fall2018.csc2017;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import fall2018.csc2017.db.DatabaseController;

/**
 * Activity that should only be run on startup to manage any initialization. Then it will run the auth app.
 */
public class InitialActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // the database needs to be initiated with a context before it can be used.
        DatabaseController.init(this);

        // switch to the auth activity right away
        Intent authenticate = new Intent(InitialActivity.this, AuthActivity.class);
        this.startActivity(authenticate);
    }

    @Override
    protected void onDestroy() {
        // we should ensure any IO we have closes properly when the app terminates.
        DatabaseController.kill();
        super.onDestroy();
    }
}
