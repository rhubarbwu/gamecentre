```
 _______ _______ _______ _______       _______  _____   _______ 
    |    |______ |_____| |  |  |       |______ |     |  |______ 
    |    |______ |     | |  |  |       |_____| |_____|  |_____| 
                                                                                                        
```
# Team Information
- Tim Duong ([tim.duong@mail.utoronto.ca](mailto:tim.duong@mail.utoronto.ca))
  - Backend Development (+ Database)
  - Testing
  - Refactoring
- Aloysius Wong ([aloysius.wong@mail.utoronto.ca](mailto:aloysius.wong@mail.utoronto.ca))
  - Abstraction Refactoring
  - Pipes
  - SlidingTiles UI
- Robert (Rupert) Wu ([wu.rupert@outlook.com](mailto:wu.rupert@outlook.com))
  - Abstraction Refactoring
  - Pipes (including duplicate refactoring, unit tests, resources)
  - General Application UI
  - SlidingTiles UI
- Guangyuan (Peter) Zhu ([guangyuan.zhu@mail.utoronto.ca](mailto:guangyuan.zhu@mail.utoronto.ca))
  - Minesweeper Developer

## Meeting History

### October 23 (Phase 1)
- Initial Database (DB) Design.
- Summarized the tasks and estimated priorities.
- Allocated duties to each member.

### November 10 (Phase 2)
- Deliberated on new tasks.
    - Tim Duong would work on refactoring existing code and add tests
- Drafted out potential changes.
- Began planning unit tests.
- Decided on the two new games to make.
    - Guangyuan Zhu would make Minesweeper
    - Robert Wu and Aloysius Wong would make pipes
    - Tim Duong would ensure that the databases work with the new games

### November 20 (Phase 2)
- Design structural improvements to old code.
- Planned to streamline more of the clunky DB calls
- Made a plan to refactor the original SlidingTiles code.
- Aloysius Wong and Robert Wu abstracted original SlidingTiles code.
- Guangyuan Zhu completed Minesweeper

### November 23 (Phase 2)
- Robert Wu will create and refactor a SlidingTiles duplicate. (completed)
- Aloysius Wong and Robert Wu will work on modifying logic and resources for Pipes. (in progress)

### November 28 (Phase 2)
- Back-navigation bugs need to be fixed in SlidingTiles and Pipes (completed 2018/11/29 by Robert Wu).
- Need to disable player interaction with a board that has already been solved (and force them to click the back button to go back to game menu) (completed 2018/11/29 by Robert Wu).

## Decisions
- After Phase 1, we decided that the Board and Tile classes should be abstracted with superclasses AbstractBoard and AbstractTile. This was to simplify the creation of future games such as Minesweeper and Pipes.
- The database should *become* a singleton as per [Room's 
  suggestion of using only a single instance of it](
https://developer.android.com/training/data-storage/room/) (as
  databases can be *very* heavy on the machine).
## Contact Info:
[Happy Fun Community Chat Server](https://discord.gg/w65GedP)

