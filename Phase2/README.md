# CSC207 Project Phase 2

## Setup instructions
1. Clone this repo (run the command `git clone https://markus.teach.cs.toronto.edu/git/csc207-2018-09-reg/group_0606`).
2. In Android Studio, open this project at `Phase2` folder.
3. Run a Gradle Sync to download dependencies [...find out how here!](https://stackoverflow.com/questions/29565263/android-studio-how-to-run-gradle-sync-manually?rq=1)
4. Build the project.
5. Run in the emulator/on your phone.

## Note on running tests
There are special instructions for coverage tests due to incompatibility between Robolectric and the built in coverage tool. See the WALKTHROUGH.pdf for additional details and instructions. Also, Level 0 for Pipes is strictly a testing level; it is not playable (or winnable).

## How to use this app
- On the startup screen, you will be prompted to login/register.
- It will then open a menu for selecting the game.
  - There are three games: minesweeper, pipes, and sliding tiles.

### Game Minesweeper
- Objective of the game: reveal tiles that are not bombs, the numbers on the tiles indicates the number of bombs are in the adjacent 8 tiles. Flag all the bombs to win.
- You will be greeted with the main menu for Minesweeper.
  - Here you can choose a board size and start a game.
  - Alternatively, you can load your previous game. If no previous game was found, a new game will be started.
  - You can open the scoreboard for scores in Minesweeper.
- Play the game when started, you can see the time taken (in seconds) and your score (which decreases as you do clicks).
  - Click on a tile to reveal it.
  - Long press on a tile to flag it as a bomb, long press again to remove the flag.
- The game will autosave so you do not lose progress.
- After you complete a game, you can return to the main menu to see the scoreboard. Here you can sort by the time taken (in ascending order) or sort by moves (in descending order). You can also search by user.

### Game Pipes
- Objective of the game: rotate the gray pipe to connect the green pipes without leaks (no openings in the circuit).
- You will be greeted with the main menu for Pipes
    - Here you can choose from one of the nine levels (of progressive difficulty).
    - You can load your saved game with the Load Saved Game button; this button will start a new game if there is no saved game.
    - You can open the scoreboard for scores in Pipes.
    - You can also load the scoreboard from this main menu.
- Play the game when started.
    - You will be greeted with a randomized board with size of the level you selected.
    - You can see the time elapsed (in seconds) and your score (decreasing with every rotation you make).
    - Clicking a gray (rotatable) pipe will rotated it clockwise by 90 degrees.
    - Clicking the Undo button will rotate the last tile you touched counter-clockwise by 90 degrees.
- The game autosaves so you don't lose progress
- When you complete a game, you can return to the main menu to see the scoreboard. Here you can sort by the time taken (in ascending order) or sort by moves (in descending order). You can also search by user.

### Game Sliding Tiles
- Objective of the game: swap tiles adjacent to the blank tile until all the tiles are sorted ascendingly.
- You will be greeted with the main menu for Sliding Tiles.
  - Here you can choose a board size and start a game.
  - Alternatively, you can load your previous game. If no previous game was found, a new game will be started.
  - You can open the scoreboard for scores in Sliding Tiles.
- Play the game when started, you can see the time taken (in seconds) and your score (which decreases as you do moves). You can also undo an infinite number of moves, each at the penalty of one point.
- The game will autosave so you do not lose progress.
- After you complete a game, you can return to the main menu to see the scoreboard. Here you can sort by the time taken (in ascending order) or sort by moves (in descending order). You can also search by user.
