\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage[colorlinks=true]{hyperref}
\usepackage{parskip}
\usepackage[left=.75in,right=.75in,top=.75in,bottom=.75in]{geometry}
\author{Aloysius, Guangyuan, Robert, \& Tim}
\title{\texttt{CSC 207} Walkthrough \\ {\large Phase 2}}

\newcommand{\clas}[1]{\item[\textbf{\texttt{#1}}]}

\begin{document}
\maketitle
\tableofcontents
\newpage
\section{Unit Testing \& Code Coverage}
\subsection{Total Coverage}

Total test coverage (not including Activities and other view-related files): 94.53\%.
Here is a list of each of the classes and their line coverage:
\begin{description}
\clas{fall2018.csc2017.AbstractBoard}	100\% (20/20)
\clas{fall2018.csc2017.AbstractTile}	77\% (7/9)
\clas{fall2018.csc2017.db.AppDatabase}	100\% (1/1)
\clas{fall2018.csc2017.db.DatabaseController}	100\% (10/10)
\clas{fall2018.csc2017.db.DatabaseDateConv}	100\% (4/4)
\clas{fall2018.csc2017.db.Gamesave}	100\% (19/19)
\clas{fall2018.csc2017.db.GamesaveController}	86\% (26/30)
\clas{fall2018.csc2017.minesweeper.BoardManager}	89\% (98/110)
\clas{fall2018.csc2017.minesweeper.MinesweeperBoard}	75\% (9/12)
\clas{fall2018.csc2017.minesweeper.MinesweeperTile}	100\% (24/24)
\clas{fall2018.csc2017.pipes.BoardManager}	96\% (53/55)
\clas{fall2018.csc2017.pipes.PipesGameBoard} 99\% (76/77)
\clas{fall2018.csc2017.pipes.PipeTile} 100\% (25/25)
\clas{fall2018.csc2017.slidingtiles.BoardManager}	100\% (76/76)
\clas{fall2018.csc2017.slidingtiles.SlidingTile} 82\% (9/11)
\clas{fall2018.csc2017.slidingtiles.SlidingTilesBoard} 87\% (27/31)
\end{description}

\subsection{Some Important Settings}

To be able to run the tests with coverage, there is an additional VM argument required. Select \textbf{Run} \textgreater\
\textbf{Edit Configurations} \textgreater\ \textbf{Android JUnit} \textgreater\ \textbf{All in app} \textgreater\
In \textbf{VM options} append the flag `-noverify` (the textbox should now have `-ea -noverify`). See GitHub issue:

\begin{itemize}
    \item \href{https://github.com/robolectric/robolectric/issues/3023#issuecomment-325365800}{\url{https://github.com/robolectric/robolectric/issues/3023\#issuecomment-325365800}} and
    \item \href{https://github.com/robolectric/robolectric/issues/3023#issuecomment-356931811}{\url{https://github.com/robolectric/robolectric/issues/3023\#issuecomment-356931811}} for additional information.\footnote{This is because \texttt{Robolectric} doesn't seem to mesh well with the integrated Android Studio coverage tests.}
\end{itemize}

\subsubsection{Robolectric}
Please make sure your system can either download or access \texttt{Robolectric} (should be automatic through Gradle sync).

\subsection{Instructions to Run Tests}

To run tests, right click on the test folder and click \textbf{Run `Tests in `fall2018.csc2017''}. Alternatively, you can run tests with coverage by clicking on \textbf{Run `Tests in `fall2018.csc2017' with Coverage'}.

\subsection{The Activities}
All of the logic (\texttt{InitialActivity} only involves passing a context) was delegated to either the database or the game code.

\subsection{Logic and Controllers}
The games mainly had the activity to class methods for the respective managers.
The database mainly uses abstraction layers over automatically generated functions (so higher level code does not involve instantiating the database or accessing the DAOs).

\pagebreak
\section{Important Classes}

\subsection{General Code}
\begin{description}
\clas{InitialActivity} The \texttt{InitialActivity} is the activity that should only be run on startup. It initializes the database then moves into the AuthActivity.
When the app terminates, it ensures the database is properly closed.
\clas{AuthActivity} The \texttt{AuthActivity} is the first activity that the user can interact with on our Game Centre,
in which the user would create an account with a unique username and password or log in with a pre-existing username/password combination.
\begin{itemize}
    \item If a username already exists in the database and the users attempts to create a new account with it, a toast will appear on the screen notifying the user to log in instead.
    \item If the user attempts to log in with a username that doesn't yet exist in the database, a toast will notify the user to register instead.
    \item If the user attempts to log in with a username that does exist in the database but with an incorrect password, a toast will notify the user that the username/password combo is incorrect and try again.
\end{itemize}
\clas{GameSelectActivity} The \texttt{GameSelectActivity} immediately follows the \texttt{AuthActivity}.
On this screen, the user chooses between playing Sliding Tiles, Minesweeper, or Pipes by clicking or tapping on the respective buttons.
\clas{ScoreboardActivity} The \texttt{ScoreboardActivity} is called by each \texttt{StartingActivity} of each game.
It displays a scoreboard that can be sorted by user and by score or time taken. More info can be found in section \ref{Scores}.
\end{description}

\subsection{The Database}
\begin{description}
\clas{AppDatabase, DatabaseController}
The \texttt{AppDatabase} class represents the actual database, which relies on the  \href{https://developer.android.com/topic/libraries/architecture/room}{Room Persistence Library}.
The \texttt{DatabaseController} is a helper class that provides access to a \textit{single instance} of the database.
This is \textit{necessary} since any form of a database instance is too resource-intensive to have multiple copies of---
so we opted to use a singleton structure
and have a single static database instance\footnote{This was suggested by the Room developers as well,
as stated \href{https://developer.android.com/training/data-storage/room/}{here}.}.
\clas{User, Score, Gamesave}
Each of these classes represent an entry in their respective tables.
\clas{UserDao, ScoreDao, GamesaveDao}
These generate the interfaces for the database and are automatically implemented by the \texttt{Room} library.
\clas{UserController, ScoreController, GamesaveController}
These classes provide a wrapper over the Daos so we can properly use the Singleton format, while being able to coordinate every
database command to (usually) one line (which helps us maintain our abstraction layer between the database and the program).
\clas{Games} This class is an enum which knows each games' IDs and headers for the scoreboard.
\end{description}

\subsection{Sliding Tiles}
\begin{description}
\clas{slidingtiles.SlidingTilesBoard, slidingtiles.SlidingTile, slidingtiles.BoardManager} These classes contain the variables to
represent and board and methods to perform board operations (such as generating a random but solvable board,
swapping tiles and checking if the board is solved).
\clas{slidingtiles.SlidingTilesStartingActivity} This is the starting activity containing the difficulty selector and the buttons
for starting and loading a game, as well as a button that opens the scoreboard activity.
\clas{slidingtiles.GameActivity} This is the activity containing the game, including showing the board, the time spent, and the score.
\end{description}

\subsection{Minesweeper}
\begin{description}
\clas{minesweeper.MinesweeperTile, minesweeper.MinesweeperBoard, minesweeper.BoardManager} These classes contain the variables to
represent a board and methods to perform board operations (such as generating a random board, revealing tiles,
and checking if the board is solved).
\clas{minesweeper.MinesweeperStartingActivity} This is the starting activity containing the difficulty selector and the buttons for starting and loading a game, as well as a button that opens the scoreboard activity.
\clas{minesweeper.GameActivity} This is the activity containing the Minesweeper game, including showing the board, the number of bombs remaining, the time spent, and the score.
\end{description}

\subsection{Pipes}
\begin{description}
\clas{pipes.PipesTile, pipes.PipesGameBoard, pipes.BoardManager} These classes contain the variables to represent a board and methods
to perform board operations (such as representing tile connections, rotating tiles, the levels, and checking if the board is solved).
\begin{itemize}
    \item PipeTile type (specifying shape, rotatability, images)
    \item Orientation (and connections with neighbours in a binary String)
    \item Rotation of tiles
    \item Levels, tile orientation randomization, score calculation
    \item Connection paths and checking if the board is solved
\end{itemize}
\clas{pipes.PipesStartingActivity} This is the activity containing the level selector and the buttons for starting a new or loaded game,
as well as a button that opens the scoreboard activity.
\clas{pipes.GameActivity} This is the activity containing the Pipes game, including showing the board (the size of which tiles vary by board size and difficulty), the time spent, and the score.
\end{description}

\pagebreak
\section{Design Patterns}

\subsection{SOLID}
\begin{enumerate}
    \item \textbf{S}ingle responsibility principle: Every class should have one responsibility. This was highly important in this
    project as the way we decided to work together was to delegate and complete tasks and check in at regular intervals to put things
    together. The Single responsibility principle minimized merge conflicts when pushing and pulling changes from git---and when merging
    branches---because (beyond committing frequently) every class served one, specific purpose. Here's an example using the Pipes game:
    \begin{itemize}
        \item \texttt{pipes.PipeTile} contains all the information of that a \texttt{PipeTile} would need: attributes such as type,
        orientation, connections; methods that return properties of the tile; constants to properly create a \texttt{PipeTile} and how
        to rotate it---but nothing more.
        \item \texttt{pipes.PipeGamesBoard} contains solely the information and methods that a \texttt{PipeGamesBoard} would need: the
        levels from which the board is built; an algorithm to create a board; other methods that return properties of the board; and an iterator
        \item \texttt{pipes.BoardManager} contains information and algorithms specific to \texttt{BoardManager}: an \texttt{ArrayDeque}
        of moves to date for the purposes of undo and algorithms such as checking for valid taps, undoing a move, getting the score,
        checking if the level is solved---generally methods that would require communication between either the \texttt{PipeGamesBoard} or the \texttt{GameActivity}.
    \end{itemize}
    This division in responsibilities between the classes not only made our code less breakable, but it also made our programming more
    efficient as we then had to resolve fewer conflicts as a result of others' changes.

    \item \textbf{O}pen/Closed principle: We implemented this practice when we decided to create abstract classes for both \texttt{Tile}
    and \texttt{Board}. Since we wanted to continue to make use of the tile code infrastructure we had built in Phase 1,
    we realized that it would be easier to inherit from a parent abstract class and implement like methods in each game
    than to create each Tile and Board class from scratch. As a result, we used the Open/Closed principle when we abstracted
    the Tile and Board classes to create \texttt{AbstractTile} and \texttt{AbstractBoard}, making it much easier for us to
    create new tile-based games---and easier for future programmers too.

    \item \textbf{L}iskov substitution principle: Similar to the Open/Closed principle, the Liskov substitution principle emphasizes the
    importance of creating proper subtypes of parent classes so that they can replace the parent classes in relevant methods. This arose
    particularly when creating the \texttt{AbstractBoard} class, in which we had to use generic type \texttt{T} which would extend from
    \texttt{AbstractTile}. This worked and allowed the Board subclasses to inherit numerous methods from the \texttt{AbstractBoard} superclass

    \item \textbf{I}nterface segregation principle: For the front end code, we did not really care \textit{how} the database works,
    as the method signatures were invariant. However, because of the clever use of \texttt{Gamesave}-, \texttt{Score}-,
    and \texttt{UserDao}, we were able to easily access the database for the purposes of saving games,
    creating and saving scores, and creating and authenticating users.

    \item \textbf{D}ependency inversion principle: Because the purposes of the different classes in each game were closely related and
    had a sort of hierarchical structure already, we didn't choose to apply much of the Dependency inversion principle and its focus on
    the creation and use of interfaces. However, we could have chosen to create interfaces that, for example, would be implemented by
    each game's \texttt{BoardManager} or \texttt{StartingActivity} classes, which would have required these classes to respectively have
    similar method names that could later be of use.

\end{enumerate}



\subsection{Singleton}
While usually frowned at as a pattern, we use a singleton structure to manage our database, as database instances are an \textbf{expensive} operation.
In addition, not using a single database instance would not make sense in any OOP perspective (as it would involve the synchronization
of multiple databases, perhaps among multiple threads --- neither of which seem particularly appealing to deal with).

\pagebreak
\section{Scores} \label{Scores}

\subsection{The Scoreboard Activity}

\begin{description}
\clas{ScoreboardActivity} This is an activity for a generic scoreboard for any game. The game for which scores are to be displayed is
determined by a extra passed into the intent named \texttt{gameId}. This activity supports sorting by score (descending), sorting by
time (ascending), or searching by user.
\end{description}

\subsection{Database Score Entry}
A \texttt{Room} database is used to store the data on user scores.

\subsection{Score Computation}
While a bit arbitrary, the score is intended \textbf{always} be inversely proportional to the number of moves.
Note however, that scores do not \textit{entirely} reflect skill --- it is also reflected by the difficulty of the puzzle generated, which is not constant.

Furthermore, it must be noted that different computations were used for different games, as their underlying structures were inherently discrete.
\end{document}
